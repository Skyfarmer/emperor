package org.emperor;

import static org.emperor.EmperorParameters.SIM_PSEUDOGRAS_NAME;

import at.ac.tuwien.ifs.sge.core.game.exception.ActionException;
import at.ac.tuwien.ifs.sge.core.util.pair.ImmutablePair;
import at.ac.tuwien.ifs.sge.core.util.pair.Pair;
import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireMovementException;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import at.ac.tuwien.ifs.sge.game.empire.model.map.EmpireTerrain;
import at.ac.tuwien.ifs.sge.game.empire.model.map.EmpireTerrainType;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnit;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.emperor.macro.GroupedActions;
import org.emperor.macro.MacroAction;
import org.emperor.macro.Scheduler;
import org.emperor.model.PhantomEnemyMap;
import org.emperor.model.PhantomEnemyUnit;

/**
 * A simulated Empire game. Allows scheduling macro events.
 */
public class SimulatedGame {

  private final static Logger log = LogManager.getLogger(SimulatedGame.class);
  private final Empire game;
  private final Scheduler scheduler;
  private final List<Pair<MacroAction, Long>> actionHistory = new ArrayList<>();
  private final Set<EmpireUnit> knownEnemyUnit = new HashSet<>();

  private final PhantomEnemyMap phantomEnemies;
  private final int playerId;


  private SimulatedGame(SimulatedGame other) {
    this.game = (Empire) other.game.copy();
    this.scheduler = other.scheduler.copy();
    this.playerId = other.playerId;
    this.phantomEnemies = other.phantomEnemies.copy();
    this.actionHistory.addAll(other.actionHistory);
    this.knownEnemyUnit.addAll(other.knownEnemyUnit);
  }

  public SimulatedGame(Empire game, Scheduler scheduler, int playerId) {
    game.clearQueue();
    this.game = game;
    this.scheduler = scheduler;
    this.playerId = playerId;
    this.phantomEnemies = new PhantomEnemyMap();

    unveilBoard(this.game);

    for (var e : game.getUnitsByPlayer((playerId + 1) % 2)) {
      knownEnemyUnit.add(new EmpireUnit(e));
    }

//    for (var e : enemyUnits) {
//      try {
//        if (this.game.getUnit(e.getId()) != null) {
//          this.game.removeDeathRelatedActionsFromQueue(e);
//        }
//        if (Utilities.isValidTile(game, e.getPosition(), e.getPlayerId())) {
//          this.game.spawnUnit(e);
//        }
//      } catch (EmpireMapException | EmpireInternalException ex) {
//        throw new RuntimeException(ex);
//      }
//    }
  }

  /**
   * Replaces tiles covered in fog-of-war with pseudogras. This is required to correctly handle the
   * simulation of undiscovered tiles.
   *
   * @param game Current game state.
   */
  private static void unveilBoard(Empire game) {
    var board = game.getBoard();
    var tiles = board.getEmpireTiles();
    for (var x = 0; x < board.getMapSize().getWidth(); x++) {
      for (var y = 0; y < board.getMapSize().getHeight(); y++) {
        if (tiles[y][x] == null) {
          // Assume every hidden tile to be grass
          var type = new EmpireTerrainType(SIM_PSEUDOGRAS_NAME, 1.0, 'g', 1);
          tiles[y][x] = new EmpireTerrain(type, new Position(x, y));
        }
      }
    }
  }

  /**
   * Add a macro action to the simulation.
   *
   * @param action Action to add.
   */
  public void addAction(MacroAction action) {
    this.scheduler.add(action);
    this.actionHistory.add(new PairWithBetterString<>(action, game.getGameClock().getGameTimeMs()));
  }

  /**
   * Advance simulation by the given parameter.
   *
   * @param ms Advance the game by this amount of ingame milliseconds.
   */
  public void advance(long ms) {
    scheduler.schedule(this.game, e -> {
      try {
        this.game.applyAction(e, game.getGameClock().getGameTimeMs() + 1);
      } catch (ActionException ex) {
        log.warn("Action failed:", ex);
      }
    });
    try {
      this.game.advance(ms);
      phantomEnemies.update(game, getEnemyId(playerId));
    } catch (ActionException e) {
      if (!shouldFilterMessage(e, game)) {
        log.warn("Action failed: {}", e.getMessage());
        log.warn("Cause: {}", e.getCause().getMessage());
        var history = new StringBuilder();
        this.actionHistory.forEach(p -> history.append(p.getA()));
        log.warn("Action History:\n{}", history);
      }
    }
  }

  public Empire getInner() {
    return this.game;
  }

  /**
   * The Empire simulation sometimes throws irrelevant exceptions. This is a workaround to
   * gracefully distinguish between actual and unnecessary exceptions.
   *
   * @param e Exception to check for.
   * @param game Current game state.
   * @return Whether this exception should be ignored.
   */
  private boolean shouldFilterMessage(ActionException e, Empire game) {
    if (e.getCause() instanceof EmpireMovementException mov) {
      var action = mov.getCausedAction();
      if (action == null) {
        return false;
      }
      var unit = game.getUnit(action.getUnitId());
      return unit == null;
    }
    return false;
  }

  /**
   * Deep copy of the simulation.
   */
  public SimulatedGame copy() {
    return new SimulatedGame(this);
  }

  /**
   * Returns a list of all valid actions in the current simulated game state.
   *
   * @param playerId Player id of the agent.
   * @return A list of possible successor moves.
   */
  public List<GroupedActions> getPossibleActions(int playerId) {
    return MacroAction.getPossibleActions(this.game, this.scheduler.getDatabase(), playerId);
  }

  public List<PhantomEnemyUnit> getPhantoms() {
    return phantomEnemies.getPhantoms();
  }

  private int getEnemyId(int playerId) {
    return (playerId + 1) % 2;
  }

  /**
   * A pair class with improved string output.
   */
  private static class PairWithBetterString<A, B> extends ImmutablePair<A, B> {

    public PairWithBetterString(A a, B b) {
      super(a, b);
    }

    @Override
    public String toString() {
      return getA() + " " + getB();
    }
  }
}

