package org.emperor.mcts;

import at.ac.tuwien.ifs.sge.game.empire.core.Empire;

public class Utils {

  private static final double LOG_BASE = 100.;

  private Utils() {
  }

  private static double logBase(double n, double base) {
    return Math.log(n) / Math.log(base);
  }

  /**
   * This function determines the threshold of when the MCTS should exploit a node even if
   * unexplored actions are still available. The goal is to decrease the threshold so that the
   * probability increases that all units receive an action and do not remain idle
   *
   * @param game Current game state.
   * @param playerId Player id.
   * @return A threshold between 0 and 1.
   */
  public static double calcExploitProb(Empire game, int playerId) {
    // The more units the less likely to exploit
    return 1 - Math.max(1.0, logBase(game.getUnitsByPlayer(playerId).size(), LOG_BASE));
  }
}
