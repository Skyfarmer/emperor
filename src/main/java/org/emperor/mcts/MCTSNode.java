package org.emperor.mcts;

import static org.emperor.EmperorParameters.MCTS_EXPLORATION_CONSTANT;
import static org.emperor.EmperorParameters.MCTS_TIME_LIMIT_MS;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.emperor.SimulatedGame;
import org.emperor.Utilities.Cache;
import org.emperor.macro.MacroAction;
import org.emperor.rollout.RolloutStrategyFactory;
import org.emperor.rollout.RolloutStrategyType;

public class MCTSNode extends FilteredRealTimeGameNode {

  public final static Logger log = LogManager.getLogger(MCTSNode.class);
  private final RolloutStrategyFactory rolloutStrategyFactory;

  private final List<MCTSNode> children = new ArrayList<>();
  private MCTSNode parent = null;
  private long visited = 0;
  private double heuristicValue = 0D;
  private final Cache<Double> cachedUcb1 = new Cache<>(this::ucb1);
  private final Cache<Optional<MCTSNode>> bestChild = new Cache<>(this::bestChild);

  public MCTSNode(int playerId, SimulatedGame game, MacroAction responsibleAction) {
    super(playerId, game, responsibleAction);
    this.rolloutStrategyFactory = new RolloutStrategyFactory();
  }

  public double getHeuristicValue() {
    return heuristicValue;
  }

  /**
   * Adds a new MCTS child to the node.
   *
   * @param child The node to add.
   */
  private void add(MCTSNode child) {
    if (child.parent != null) {
      throw new RuntimeException("Node already has a parent");
    }
    child.parent = this;
    this.children.add(child);
    //Invalidate cache because we might have a better candidate now.
    this.bestChild.invalidate();
  }


  /**
   * Expand node to add an unexplored action. Node is added as a child.
   *
   * @return The newly expanded node.
   */
  private MCTSNode expand() {
    var action = this.popAction();
    if (action.isEmpty()) {
      throw new RuntimeException("Action should not be empty");
    }
    var copy = this.getGame().copy();
    var a = action.get();
    // We need a copy here because this action will be abused for the simulation.
    copy.addAction(a.copy());

    var s = new MCTSNode(this.getPlayerId(), copy, a);
    if (!s.hasRemainingActions()) {
      s.advanceUntilActionsAvailable();
    }
    this.add(s);

    return s;
  }

  /**
   * Run the simulation using the given rollout type.
   *
   * @param rolloutType The rollout strategy to use.
   * @return The heuristic value determined by the simulation.
   */
  private double rolloutWithStrategy(RolloutStrategyType rolloutType) {
    log.trace("Rollout for action {}", this.getResponsibleAction());
    return rolloutStrategyFactory.createRolloutStrategy(rolloutType)
        .simulation(this.getGame().copy(), this.getPlayerId());
  }

  /**
   * @return The deepth of the search tree.
   */
  public long treeDepth() {
    var max = this.children.stream().map(MCTSNode::treeDepth).max(Long::compare).orElse(0L);
    return 1 + max;
  }

  /**
   * Propagate the given heuristic value back through the search tree.
   *
   * @param heuristic_value The value to propagate.
   */
  private void backPropagate(double heuristic_value) {
    this.visited += 1;
    this.heuristicValue += heuristic_value;
    this.cachedUcb1.invalidate();

    if (this.parent != null) {
      this.parent.backPropagate(heuristic_value);
    }
  }

  /**
   * Calculates the UCB1 value.
   *
   * @return UCB1 value.
   */
  private double ucb1() {
    var n = 2 * Math.log(this.parent.visited);
    var visited = Math.max(1, this.visited);
    var tmp = Math.sqrt(n / visited);

    var average_reward = this.heuristicValue / (this.children.size() + 1);

    return average_reward + MCTS_EXPLORATION_CONSTANT * tmp;
  }

  /**
   * Returns the child with the largest UCB1 value.
   *
   * @return Child with largest UCB1 value.
   */
  private Optional<MCTSNode> bestChild() {
    return this.children.stream().max(Comparator.comparingDouble(node -> node.cachedUcb1.get()));
  }

  /**
   * Selection takes either the best child node for exploitation or expands the tree using an
   * explored action. This function deviates from classical MCTS algorithms by also exploiting a
   * child node by calculting a certain threshold which depends on the number of units the agent
   * commands.
   *
   * @return Either newly expanded or best child node.
   */
  // Also called tree-policy
  private MCTSNode selection() {
    var prob = ThreadLocalRandom.current().nextDouble();
    var threshold = Utils.calcExploitProb(this.getGame().getInner(), this.getPlayerId());
    if (this.hasRemainingActions() && prob > threshold) {
      //Not all actions explored yet
      return this.expand();
    } else {
      var best = this.bestChild();
      if (best.isPresent()) {
        return best.get().selection();
      } else {
        return this;
      }
    }
  }

  /**
   * Runs the MCTS search for a configurable amount of Wall-Clock time.
   *
   * @return A list of non-conflicting best child nodes.
   */
  public List<MCTSNode> utcSearch() {
    var start = System.currentTimeMillis();
    var finish = System.currentTimeMillis();
    while (finish - start < MCTS_TIME_LIMIT_MS) {
      var selected = this.selection();
      var value = selected.rolloutWithStrategy(RolloutStrategyType.combined);
      if (value < -1.0 || value > 1.0) {
        log.warn("Value {} not normalized", value);
      }
      selected.backPropagate(value);

      finish = System.currentTimeMillis();
    }
    var candidates = new ArrayList<MCTSNode>();
    while (true) {
      var c = this.bestChild().orElse(this);
      if (c == this) {
        // No child nodes remain, break out...
        break;
      }
      candidates.add(c);
      // Remove all child nodes that use the same unit and/or city as the child node determined as the best one.
      this.children.removeIf(n -> {
        var unitConflict = n.getResponsibleAction().affectedUnit()
            .equals(c.getResponsibleAction().affectedUnit());
        var cityConflict = c.getResponsibleAction().affectedCity()
            .flatMap(affectedCity -> n.getResponsibleAction().affectedCity()
                .map(affectedCity::equals)).orElse(false);

        return unitConflict || cityConflict;
      });
    }

    return candidates;
  }
}
