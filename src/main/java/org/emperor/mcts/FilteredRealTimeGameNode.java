package org.emperor.mcts;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.emperor.EmperorParameters;
import org.emperor.SimulatedGame;
import org.emperor.macro.GroupedActions;
import org.emperor.macro.MacroAction;

/**
 * The filtered game node represents basic functionality for outstanding actions in the current game
 * setting.
 */
public class FilteredRealTimeGameNode {

  private final static Logger log = LogManager.getLogger(FilteredRealTimeGameNode.class);
  private final int playerId;
  private final SimulatedGame game;
  private final List<GroupedActions> possibleActions;
  private final MacroAction responsibleAction;
  private final List<UUID> avoidUnits = new ArrayList<>();

  private boolean hasAdvanced = false;

  public FilteredRealTimeGameNode(int playerId, SimulatedGame game, MacroAction responsibleAction) {
    this.playerId = playerId;
    this.game = game;
    this.responsibleAction = responsibleAction;
    var possibleActions = game.getPossibleActions(playerId);
    this.possibleActions = new ArrayList<>(possibleActions);
  }


  /**
   * Advances the game time until actions are available again.
   */
  protected void advanceUntilActionsAvailable() {
    if (hasAdvanced || !this.possibleActions.isEmpty()) {
      throw new RuntimeException("Node already has actions available");
    }
    hasAdvanced = true;
    for (var i = 0; i < EmperorParameters.MCTS_MAX_ADVANCE_STEPS; i++) {
      game.advance(EmperorParameters.MCTS_ADVANCE_PRECISION_MS);
      var possibleActions = game.getPossibleActions(playerId);
      if (!possibleActions.isEmpty()) {
        this.possibleActions.addAll(possibleActions);
        return;
      }
    }
    log.warn("Advance limit reached for node {}", this);
  }

  /**
   * Return and remove a new action from the remaining list.
   *
   * @return A new action.
   */
  protected Optional<MacroAction> popAction() {
    var action = GroupedActions.getRandomAction(this.possibleActions, avoidUnits);
    action.ifPresent(macroAction -> avoidUnits.add(macroAction.affectedUnit()));
    return action;
  }

  public boolean hasRemainingActions() {
    return !this.possibleActions.isEmpty();
  }

  protected SimulatedGame getGame() {
    return this.game;
  }

  protected int getPlayerId() {
    return this.playerId;
  }

  public MacroAction getResponsibleAction() {
    return this.responsibleAction;
  }
}
