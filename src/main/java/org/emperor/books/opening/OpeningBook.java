package org.emperor.books.opening;

import at.ac.tuwien.ifs.sge.core.engine.communication.ActionResult;
import at.ac.tuwien.ifs.sge.core.game.exception.ActionException;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.emperor.ActionDatabase;
import org.emperor.EmperorAgent;

/**
 * This models the agent's open book which consists of action entries that are executed on start up.
 * Thereby, the actions may depend on each other, which is modelled by dependencies in a list-linked
 * fashion.
 */
public class OpeningBook {

    public final static Logger log = LogManager.getLogger(OpeningBook.class);
    // collect units blocked by the currently played entry to prevent mcts from commanding them
    private final ActionDatabase actionDatabase;
    private final Queue<OpeningBookEntry> pendingEntries;
    private final List<OpeningBookEntry> currentEntries;
    private EmperorAgent agent;

    public OpeningBook(EmperorAgent agent, ActionDatabase database) {
        this.pendingEntries = new LinkedList<>();
        this.currentEntries = new ArrayList<>();
        this.agent = agent;
        this.actionDatabase = database;
    }

    public void addEntry(OpeningBookEntry entry) {
        this.pendingEntries.add(entry);
    }

    /**
     * start an action by marking employed units as "busy".
     */
    private void startAction(OpeningBookEntry entry) throws ActionException {
        log.debug("NUMBER OF ENTRIES: {}", pendingEntries.size());
        if (entry != null) {
            for (var u : entry.getEmployedUnitIds()) {
                actionDatabase.remove(u);
            }
        }
        if (entry != null) {
            for (var u : entry.getEmployedUnitIds()) {
                actionDatabase.add(u);
            }
        }
    }

    public void start() {
        this.resume();
    }

    /**
     * depending on the state of dependencies, apply pending book entries if applicable (e.g. unit is
     * available)
     */
    private void resume() {
        log.trace("resume opening book");
        var game = agent.getGame();
        pendingEntries.forEach(action -> {
            try {
                if (action.isApplicable(game)) {
                    this.startAction(action);
                    this.currentEntries.add(action);
                    this.pendingEntries.remove(action);
                    action.execute(game, agent, this.actionDatabase);
                }
            } catch (ActionException e) {
                log.error("cannot be handled: ", e);
            }
        });
        this.currentEntries.stream().filter(a -> a.isDoneIn(game))
            .map(OpeningBookEntry::getEmployedUnitIds)
            .flatMap(List::stream)
            .forEach(actionDatabase::remove);
        this.currentEntries.removeAll(
            currentEntries.stream().filter(a -> a.isDoneIn(game)).toList());
        if (currentEntries.isEmpty() && pendingEntries.isEmpty()) {
            this.cleanup();
        }
    }

    /**
     * recognize game updates and depending on the game state proceed with new entries
     *
     * @param action occurred
     */
    public void onGameUpdate(EmpireEvent action, ActionResult result) {
        this.currentEntries.forEach(e -> e.onGameUpdate(action, result));
        this.resume();
    }

    /**
     * called by agent
     */
    private void cleanup() {
        this.agent.releaseOpeningBook();
        this.agent = null;
    }
}
