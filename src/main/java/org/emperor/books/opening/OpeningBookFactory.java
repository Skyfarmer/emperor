package org.emperor.books.opening;

import java.util.List;
import org.emperor.ActionDatabase;
import org.emperor.EmperorAgent;
import org.emperor.Utilities.UnitTypes;

/**
 * factory that facilitates the creation of opening books that follow different strategies labelled
 * by the OpeningBookStrategy enum.
 */
public class OpeningBookFactory {

  public OpeningBook createOpeningBook(OpeningBookStrategy strategy, EmperorAgent agent,
      ActionDatabase db) {
    switch (strategy) {
      case produceAndExplore:
        return composeProduceAndExplore(agent, db);
      default:
        return new OpeningBook(agent, db);
    }
  }

  /**
   * create specific opening book that produces a new unit and sends it to a reasonable position.
   *
   * @param db of units
   * @return produceAndExplore opening book
   */
  private OpeningBook composeProduceAndExplore(EmperorAgent agent, ActionDatabase db) {
    OpeningBook openingBook = new OpeningBook(agent, db);
    // first entry - produce
    var unit = agent.getGame().getUnitsByPlayer(agent.getPlayerId()).stream().findAny()
        .orElse(null);
    if (unit != null) {
      var city = agent.getGame().getCity(unit.getPosition());
      if (city != null) {
        var production1 = new OpeningBookProductionEntry(city.getPosition(),
            UnitTypes.Scout.getValue(), unit.getId(), List.of());
        openingBook.addEntry(production1);
//        var production2 = new OpeningBookProductionEntry(city.getPosition(),
//            unit.getUnitTypeId(), unit.getId(), List.of(production1));
//        openingBook.addEntry(production2);
//        var production3 = new OpeningBookProductionEntry(city.getPosition(),
//            unit.getUnitTypeId(), unit.getId(), List.of(production1, production2));
//        openingBook.addEntry(production3);

      }
    }
    // second entry - explore
    //openingBook.addEntry(new OpeningBookExplorationEntry(agent));
    return openingBook;
  }
}
