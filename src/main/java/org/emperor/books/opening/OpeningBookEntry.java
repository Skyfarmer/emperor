package org.emperor.books.opening;

import at.ac.tuwien.ifs.sge.core.engine.communication.ActionResult;
import at.ac.tuwien.ifs.sge.core.game.exception.ActionException;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;
import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import java.util.List;
import java.util.UUID;
import org.emperor.ActionDatabase;
import org.emperor.EmperorAgent;

/**
 * opening book entry that represents an arbitrary action
 */
public interface OpeningBookEntry  {

    List<EmpireEvent> getCorrespondingAction();

    //block units that are employed in this entry for other agent tasks (e.g. mcts) when run concurrently
    List<UUID> getEmployedUnitIds();

    //define rule-based or event-based definition of success of this entry
    boolean isDoneIn(Empire game);

    //set event-based status of this entry (e.g. success)
    void onGameUpdate(EmpireEvent action, ActionResult result);

    boolean isApplicable(Empire game);

    void execute(Empire game, EmperorAgent agent, ActionDatabase database) throws ActionException;

    //get opening book entries which this entry depends on (need to be processed earlier)
    List<OpeningBookEntry> getDependencies();
}
