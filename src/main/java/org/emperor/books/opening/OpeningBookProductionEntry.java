package org.emperor.books.opening;

import at.ac.tuwien.ifs.sge.core.engine.communication.ActionResult;
import at.ac.tuwien.ifs.sge.core.game.exception.ActionException;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.ProductionAction;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start.ProductionStartOrder;
import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.emperor.ActionDatabase;
import org.emperor.EmperorAgent;

/**
 * opening book entry that models the production of a new unit at a given city position.
 */
public class OpeningBookProductionEntry implements OpeningBookEntry {

  public final static Logger log = LogManager.getLogger(OpeningBookProductionEntry.class);
  private final UUID unitId;
  private final Position cityPosition;
  private final int unitTypeId;
  private List<EmpireEvent> actions;
  private boolean fulfilled = false;
  private boolean needsRestart = false;
  private List<OpeningBookEntry> dependencies;


  public OpeningBookProductionEntry(Position cityPosition,
      int unitTypeId,
      UUID unitId,
      List<OpeningBookEntry> dependencies) {
    this.actions = new ArrayList<>();
    this.unitId = unitId;
    this.unitTypeId = unitTypeId;
    this.cityPosition = cityPosition;
    this.actions.add(new ProductionStartOrder(cityPosition, 2));
    this.dependencies = new ArrayList<>();
    this.dependencies.addAll(dependencies);
  }

  @Override
  public List<EmpireEvent> getCorrespondingAction() {
    return actions;
  }

  @Override
  public List<UUID> getEmployedUnitIds() {
    return List.of(unitId);
  }

  @Override
  public synchronized boolean isDoneIn(Empire game) {
    return fulfilled;
  }

  @Override
  public void onGameUpdate(EmpireEvent action, ActionResult result) {
    if (action instanceof ProductionAction success) {
      if (success.getCityPosition().equals(this.cityPosition) && result.wasSuccessful()) {
        setFulfilled();
      } else if (!result.wasSuccessful()) {
        this.setNeedsRestart();
      }
    }
    log.trace("class of: {}", action.getClass().getName());
    log.trace(action.toString());
  }

  @Override
  public boolean isApplicable(Empire game) {
    return this.dependencies.stream().map(d -> d.isDoneIn(game)).reduce((d1, d2) -> d1 & d2)
        .orElse(true);
  }

  private void setNeedsRestart() {
    this.needsRestart = true;
  }

  public boolean needsRestart() {
    return needsRestart;
  }

  private synchronized void setFulfilled() {
    this.fulfilled = true;
  }

  public void execute(Empire game, EmperorAgent agent, ActionDatabase db) throws ActionException {
    for (var action : this.actions) {
      log.info("Start action: {}", action.toString());
      game.applyAction(action, game.getGameClock().getGameTimeMs() + 1);
      agent.sendAction(action, System.currentTimeMillis() + 50);
    }
  }

  @Override
  public List<OpeningBookEntry> getDependencies() {
    return dependencies;
  }

}
