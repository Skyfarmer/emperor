package org.emperor.books.opening;

import at.ac.tuwien.ifs.sge.core.engine.communication.ActionResult;
import at.ac.tuwien.ifs.sge.core.game.exception.ActionException;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.MovementAction;
import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.emperor.ActionDatabase;
import org.emperor.EmperorAgent;

/**
 * Opening book entry that aims to send an arbitrary, available unit to positions that seem
 * reasonable (e.g. corners of the map) -> replaced by routing action
 */
public class OpeningBookExplorationEntry implements OpeningBookEntry {

    public final static Logger log = LogManager.getLogger(OpeningBookExplorationEntry.class);

    private final EmperorAgent agent;
    private boolean fulfilled = false;
    private List<UUID> unitIds = new ArrayList<>();
    private List<EmpireEvent> actions = new ArrayList<>();
    private List<OpeningBookEntry> dependencies;

    public OpeningBookExplorationEntry(EmperorAgent agent, List<OpeningBookEntry> dependencies) {
        this.agent = agent;
        this.dependencies = new ArrayList<>();
        this.dependencies.addAll(dependencies);
    }

    @Override
    public List<EmpireEvent> getCorrespondingAction() {
        return actions;
    }

    @Override
    public List<UUID> getEmployedUnitIds() {
        return unitIds;
    }

    @Override
    public boolean isDoneIn(Empire game) {
        return fulfilled;
    }

    @Override
    public void onGameUpdate(EmpireEvent action, ActionResult result) {
        if (action instanceof MovementAction success) {
            if (unitIds.contains(success.getUnitId()) && result.wasSuccessful()) {
                fulfilled = true;
            }
        }
    }

    @Override
    public boolean isApplicable(Empire game) {
        return this.dependencies.stream().map(d -> d.isDoneIn(game)).reduce((d1, d2) -> d1 & d2)
            .orElse(true);
    }

    @Override
    public void execute(Empire game, EmperorAgent agent, ActionDatabase db) throws ActionException {
        var unit = agent.getAvailableUnits(db).get(0);
        if (unit == null) {
            return;
        }
        for (EmpireEvent action : this.actions) {
            log.info("Start action: " + action.toString());
            game.applyAction(action, game.getGameClock().getGameTimeMs() + 1);
            agent.sendAction(action, System.currentTimeMillis() + 50);
        }
    }

    @Override
    public List<OpeningBookEntry> getDependencies() {
        return this.dependencies;
    }
}
