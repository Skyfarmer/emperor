package org.emperor.rollout;

import java.util.concurrent.ThreadLocalRandom;
import org.emperor.SimulatedGame;

public class RandomRewardStrategy implements RolloutStrategy {

  @Override
  public double simulation(SimulatedGame game, int playerId) {
    return ThreadLocalRandom.current().nextDouble();
  }
}
