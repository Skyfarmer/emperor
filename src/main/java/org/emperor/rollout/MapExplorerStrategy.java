package org.emperor.rollout;

import static org.emperor.EmperorParameters.MAP_EXP_SIMULATION_DURATION_MS;
import static org.emperor.EmperorParameters.MAP_EXP_SIMULATION_PRECISION_MS;
import static org.emperor.EmperorParameters.SIM_PSEUDOGRAS_NAME;

import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireMapException;
import at.ac.tuwien.ifs.sge.game.empire.map.EmpireMap;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnit;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.emperor.SimulatedGame;
import org.emperor.macro.GroupedActions;

public class MapExplorerStrategy implements RolloutStrategy {

  public final static Logger log = LogManager.getLogger(MapExplorerStrategy.class);
  private static long MAX_NEEDED_TIME = 0;

  private static double getHeuristicValue(EmpireMap board, int uncoveredTiles, int additionalUnits,
      int killedUnits,
      int timeMs) {
    if (timeMs == 0) {
      return 0;
    }
    if (timeMs > MAX_NEEDED_TIME) {
      MAX_NEEDED_TIME = timeMs + 1;
    }
    var totalTiles = board.getMapSize().getWidth() * board.getMapSize().getHeight();
    var ratio = (double) uncoveredTiles / totalTiles;
    ratio += additionalUnits + killedUnits;
    //var counterWeight = (MAX_NEEDED_TIME - timeMs) / (double) MAX_NEEDED_TIME;
    //var r = ratio * counterWeight;
    log.trace("{}/{}/{}", ratio, uncoveredTiles, totalTiles);
    return ratio;
  }

  private static List<Position> getUncoveredPositions(Empire game, int playerId) {
    return game.getBoard().getActiveVisionByPosition().entrySet().stream()
        .filter(p -> p.getValue()[playerId].hasVision()).map(Entry::getKey).filter(p -> {
          try {
            var tile = game.getBoard().getTile(p);
            return tile.getName().equals(SIM_PSEUDOGRAS_NAME);
          } catch (EmpireMapException e) {
            throw new RuntimeException(e);
          }
        }).collect(Collectors.toList());
  }

  private static List<EmpireUnit> getEnemyUnits(Empire game, int me) {
    return game.getUnitsByPlayer((me + 1) % 2);
  }

  public void addAction(SimulatedGame game, int playerId) {
    var possibleActions = game.getPossibleActions(playerId);
    // TODO AVOID as well?
    var selectedAction = GroupedActions.getRandomAction(possibleActions, new ArrayList<>());
    selectedAction.ifPresent(game::addAction);
  }


  @Override
  public double simulation(SimulatedGame game, int playerId) {
    var accumulatedTime = 0;
    Set<Position> uncoveredTiles = new HashSet<>();
    int amountUnits = game.getInner().getUnitsByPlayer(playerId).size();
    var enemyUnits = getEnemyUnits(game.getInner(), playerId).size();

    while (accumulatedTime < MAP_EXP_SIMULATION_DURATION_MS) {
      //TODO ADD MULTIPLE OPTIONS
      this.addAction(game, playerId);
      //this.addAction(game, (playerId + 1) % 2);

      accumulatedTime += MAP_EXP_SIMULATION_PRECISION_MS;
      game.advance(MAP_EXP_SIMULATION_PRECISION_MS);

      var active = getUncoveredPositions(game.getInner(), playerId);
      uncoveredTiles.addAll(active);
    }

    var currentUnits = game.getInner().getUnitsByPlayer(playerId).size();
    var currentEnemyUnits = getEnemyUnits(game.getInner(), playerId).size();
    return getHeuristicValue(game.getInner().getBoard(), uncoveredTiles.size(),
        currentUnits - amountUnits, currentEnemyUnits - enemyUnits, accumulatedTime);
  }
}
