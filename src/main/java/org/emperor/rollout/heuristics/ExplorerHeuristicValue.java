package org.emperor.rollout.heuristics;

import static org.emperor.EmperorParameters.SIM_PSEUDOGRAS_NAME;

import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireMapException;
import at.ac.tuwien.ifs.sge.game.empire.map.EmpireMap;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import java.util.List;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import org.emperor.SimulatedGame;

/**
 * This heuristic is meant to motivate the agent to explore the map and thereby discover new cities
 * and the enemy.
 */
public class ExplorerHeuristicValue implements HeuristicValue {

  private static long MAX_NEEDED_TIME = 0;

  private final double weight;

  public ExplorerHeuristicValue(double weight) {
    this.weight = weight;
  }

  private static List<Position> getUncoveredPositions(Empire game, int playerId) {
    return game.getBoard().getActiveVisionByPosition().entrySet().stream()
        .filter(p -> p.getValue()[playerId].hasVision()).map(Entry::getKey).filter(p -> {
          try {
            var tile = game.getBoard().getTile(p);
            return tile.getName().equals(SIM_PSEUDOGRAS_NAME);
          } catch (EmpireMapException e) {
            throw new RuntimeException(e);
          }
        }).collect(Collectors.toList());
  }

  @Override
  public double getCoeff() {
    return weight;
  }

  @Override
  public double getHeuristicRatio(SimulatedGame oldState, SimulatedGame newState, int timeMs,
      int playerId) {
    return getHeuristicValue(newState.getInner(), timeMs, playerId);
  }

  private double getHeuristicValue(Empire game, int timeMs, int playerId) {
    if (timeMs == 0 || game == null || game.getBoard() == null) {
      return 0;
    }
    EmpireMap board = game.getBoard();
    if (timeMs > MAX_NEEDED_TIME) {
      MAX_NEEDED_TIME = timeMs + 1;
    }
    double totalTiles = game.getBoard().getMapSize().getWidth() * board.getMapSize().getHeight();
    return getUncoveredPositions(game, playerId).size() / Math.max(1, totalTiles);
  }
}
