package org.emperor.rollout.heuristics;

import org.emperor.SimulatedGame;

/**
 * This models a generic heuristic ratio (0..1) that can be weighted by a coefficient
 */
public interface HeuristicValue {

  /**
   * @return weight coeff
   */
  double getCoeff();

  /**
   * @return heuristic value between -1 and 1
   */
  double getHeuristicRatio(SimulatedGame oldState, SimulatedGame newState, int timeMs,
      int playerId);

}
