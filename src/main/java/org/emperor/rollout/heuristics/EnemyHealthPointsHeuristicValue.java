package org.emperor.rollout.heuristics;

import org.emperor.SimulatedGame;

/**
 * This heuristic values an action based on the amount of enemy health points. The more enemy health
 * points, the worse for the agent.
 */
public class EnemyHealthPointsHeuristicValue extends HealthPointsHeuristicValue {

  public EnemyHealthPointsHeuristicValue(double weight) {
    super(weight);
  }

  @Override
  public double getHeuristicRatio(SimulatedGame oldState, SimulatedGame newState,
      int timeMs, int playerId) {
    var enemy = (playerId + 1) % 2;
    // enemy health points are evaluated negatively
    return -super.getHeuristicRatio(oldState, newState, timeMs, enemy);
  }

}
