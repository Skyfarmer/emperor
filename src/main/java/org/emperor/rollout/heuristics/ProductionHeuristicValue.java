package org.emperor.rollout.heuristics;

import org.emperor.SimulatedGame;

/**
 * This heuristic values the number of newly produced units and thus is meant to motivate the agent
 * to seize cities and produce new cities.
 */
public class ProductionHeuristicValue implements HeuristicValue {

  private static final long PRODUCTION_ESTIMATION_OFFSET = 2;
  private final double weight;

  public ProductionHeuristicValue(double weight) {
    this.weight = weight;
  }

  @Override
  public double getCoeff() {
    return this.weight;
  }

  @Override
  public double getHeuristicRatio(SimulatedGame oldState, SimulatedGame newState, int timeMs,
      int playerId) {
    var oldUnits = oldState.getInner().getUnitsByPlayer(playerId);
    var newUnits = newState.getInner().getUnitsByPlayer(playerId).stream()
        .filter(u -> !oldUnits.contains(u))
        .count();

    var possibleProduction = (oldUnits.size() + PRODUCTION_ESTIMATION_OFFSET) / 2;

    return (double) newUnits / Math.max(newUnits, possibleProduction);
  }
}
