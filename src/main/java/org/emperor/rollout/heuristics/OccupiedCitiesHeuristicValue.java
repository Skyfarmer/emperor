package org.emperor.rollout.heuristics;

import org.emperor.SimulatedGame;

/**
 * This heuristic is meant to animate the agent to seize cities that the agent is already aware of.
 * As a consequence, the agent might use these cities to produce new units.
 */
public class OccupiedCitiesHeuristicValue implements HeuristicValue {

  private final double weight;

  public OccupiedCitiesHeuristicValue(double weight) {
    this.weight = weight;
  }

  @Override
  public double getCoeff() {
    return weight;
  }

  @Override
  public double getHeuristicRatio(SimulatedGame oldState, SimulatedGame newState, int timeMs,
      int playerId) {
    if (newState == null || newState.getInner() == null) {
      return 0D;
    }
    // use oldState here, otherwise agent might avoid to find new cities
    return getNumberOfOccupiedCities(newState, playerId) /
        (double) Math.max(1, oldState.getInner().getCitiesByPosition().size());
  }

  private int getNumberOfOccupiedCities(SimulatedGame game, int playerId) {
    int occupied = 0;
    for (var city : game.getInner().getCitiesByPosition().values()) {
      if (city.getPlayerId() == playerId) {
        occupied += 1;
      }
    }
    return occupied;
  }
}
