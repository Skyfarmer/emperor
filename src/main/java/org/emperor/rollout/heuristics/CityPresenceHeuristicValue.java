package org.emperor.rollout.heuristics;

import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnit;
import java.util.HashMap;
import java.util.List;
import org.emperor.SimulatedGame;
import org.emperor.model.PhantomEnemyUnit;

/**
 * This heuristic employs the phantom enemy map maintained in SimulatedGame. This heuristic's
 * purpose is to achieve unit predominance around all cities within a specific radius. This is done
 * by computing the (probable) positions of all known enemy units and their strength and comparing
 * it to that of the own units.
 */
public class CityPresenceHeuristicValue implements HeuristicValue {

  private static final int PRESENCE_RADIUS = 3;
  private final double weight;

  public CityPresenceHeuristicValue(double weight) {
    this.weight = weight;
  }

  @Override
  public double getCoeff() {
    return weight;
  }

  @Override
  public double getHeuristicRatio(SimulatedGame oldState, SimulatedGame newState, int timeMs,
      int playerId) {
    if (newState == null || newState.getInner() == null) {
      return 0D;
    }
    List<Position> cityPositions = newState.getInner().getCitiesByPosition().keySet().stream()
        .toList();
    if (cityPositions.isEmpty()) {
      return 0;
    }
    double enemyPresence = 0D;
    double unitPresence = 0D;
    for (Position cityPos : cityPositions) {
      enemyPresence += enemyPresenceFactorFor(cityPos, newState);
      unitPresence += unitPresenceFactorFor(cityPos, newState, playerId);
    }
    var r = (unitPresence - enemyPresence) / cityPositions.size() / (unitPresence + enemyPresence);
    if (Double.isNaN(r)) {
      return 0.0;
    } else {
      return r;
    }
  }

  /**
   * This accumulates the strength of each known phantom enemy per tile and thus returns a map of
   * potential enemy strength
   *
   * @param newState after the action to evaluate is executed
   * @return map of phantom strength
   */
  private HashMap<Position, Double> getCumulatedPhantomStrength(SimulatedGame newState) {
    HashMap<Position, Double> phantomStrengths = new HashMap<>();
    var game = newState.getInner();
    for (PhantomEnemyUnit unit : newState.getPhantoms()) {
      for (Position position : unit.getPotentialFields(game.getBoard(),
          game.getGameClock().getGameTimeMs())) {
        if (phantomStrengths.get(position) != null) {
          phantomStrengths.put(position, phantomStrengths.get(position) + unit.getPotentialStrength(
              newState.getInner().getBoard(), game.getGameClock().getGameTimeMs()));
        } else {
          phantomStrengths.put(position, unit.getPotentialStrength(game.getBoard(),
              game.getGameClock().getGameTimeMs()));
        }
      }
    }
    return phantomStrengths;
  }

  private double enemyPresenceFactorFor(Position position, SimulatedGame newState) {
    double presence = 0;
    // enhance computation performance by precomputing the whole phantomMap
    HashMap<Position, Double> presenceMap = getCumulatedPhantomStrength(newState);
    for (int x = position.getX() - PRESENCE_RADIUS; x <= position.getX() + PRESENCE_RADIUS; x++) {
      for (int y = position.getY() - PRESENCE_RADIUS; y <= position.getY() + PRESENCE_RADIUS; y++) {
        Position pos = new Position(x, y);
        if (newState.getInner().getBoard().isInside(pos)) {
          presence += presenceMap.get(pos) == null ? 0 : presenceMap.get(pos);
        }
      }
    }
    return presence;
  }

  /**
   * Calculate the presence of the own units around the position given.
   *
   * @param position of city.
   * @param newState after modelling execution of action to evaluate
   * @param playerId of agent
   * @return number of own units around a city
   */
  private double unitPresenceFactorFor(Position position, SimulatedGame newState, int playerId) {
    double presence = 0;
    List<Position> unitPositions = newState.getInner().getUnitsByPlayer(playerId).stream()
        .map(EmpireUnit::getPosition).toList();
    for (int x = position.getX() - PRESENCE_RADIUS; x <= position.getX() + PRESENCE_RADIUS; x++) {
      for (int y = position.getY() - PRESENCE_RADIUS; y <= position.getY() + PRESENCE_RADIUS; y++) {
        Position pos = new Position(x, y);
        if (newState.getInner().getBoard().isInside(pos) && unitPositions.contains(pos)) {
          // as position of own units is always known, use discrete values for presence instead of probability values
          presence++;
        }
      }
    }
    return presence;
  }
}
