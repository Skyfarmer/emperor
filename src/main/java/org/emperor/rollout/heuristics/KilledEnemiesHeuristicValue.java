package org.emperor.rollout.heuristics;

import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnit;
import org.emperor.SimulatedGame;

/**
 * This heuristic weights the number of killed enemy units and thus animates the agent to start and
 * win battles.
 */
public class KilledEnemiesHeuristicValue implements HeuristicValue {

  private final double weight;

  public KilledEnemiesHeuristicValue(double weight) {
    this.weight = weight;
  }

  @Override
  public double getCoeff() {
    return weight;
  }

  @Override
  public double getHeuristicRatio(SimulatedGame oldState, SimulatedGame newState, int timeMs,
      int playerId) {
    if (newState == null || newState.getInner() == null || oldState == null
        || oldState.getInner() == null) {
      return 0.0;
    }
    int enemyId = getEnemyId(newState.getInner(), playerId);
    if (getNumberOfUnits(oldState, enemyId) == 0) {
      return 0;
    } else {
      int deadCount = 0;
      for (EmpireUnit enemy : oldState.getInner().getUnitsByPlayer(enemyId)) {
        EmpireUnit enemyNew = newState.getInner().getUnit(enemy.getId());
        if (enemyNew != null && !enemyNew.isAlive()) {
          deadCount++;
        }
      }
      return deadCount / (double) getNumberOfUnits(oldState, enemyId);
    }
  }

  private int getEnemyId(Empire game, int playerId) {
    for (int i = 0; i < game.getNumberOfPlayers(); i++) {
      if (playerId == i) {
        continue;
      }
      return i;
    }
    return 0;
  }

  private int getNumberOfUnits(SimulatedGame game, int playerId) {
    return game.getInner().getUnitsByPlayer(playerId).size();
  }
}
