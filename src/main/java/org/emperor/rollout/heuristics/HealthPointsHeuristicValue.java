package org.emperor.rollout.heuristics;

import at.ac.tuwien.ifs.sge.core.util.pair.ImmutablePair;
import at.ac.tuwien.ifs.sge.core.util.pair.Pair;
import org.emperor.SimulatedGame;

/**
 * This heuristic values an action based on the amount of health points. The more health points
 * summed over all units, the better for the agent.
 */
public class HealthPointsHeuristicValue implements HeuristicValue {

  private final double weight;

  public HealthPointsHeuristicValue(double weight) {
    this.weight = weight;
  }

  @Override
  public double getCoeff() {
    return weight;
  }

  @Override
  public double getHeuristicRatio(SimulatedGame oldState, SimulatedGame newState, int timeMs,
      int playerId) {
    var hp = getHealthPoints(oldState, newState, playerId);
    return (hp.getA() - hp.getB()) / Math.max(1., hp.getB() + hp.getA());
  }

  private Pair<Integer, Integer> getHealthPoints(SimulatedGame oldState, SimulatedGame newState,
      int playerId) {
    if (oldState == null || oldState.getInner() == null) {
      return new ImmutablePair<>(0, 0);
    }

    return newState.getInner().getUnitsByPlayer(playerId).stream()
        .filter(u -> oldState.getInner().getUnit(u.getId()) != null)
        .map(u -> new ImmutablePair<>(u.getHp(), oldState.getInner().getUnit(u.getId()).getHp()))
        .reduce((v1, v2) -> new ImmutablePair<>(v1.getA() + v2.getA(), v1.getB() + v2.getB()))
        .orElse(new ImmutablePair<>(0, 0));
  }
}
