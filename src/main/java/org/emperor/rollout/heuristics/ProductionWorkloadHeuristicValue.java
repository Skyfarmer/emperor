package org.emperor.rollout.heuristics;

import at.ac.tuwien.ifs.sge.game.empire.model.map.EmpireCity;
import at.ac.tuwien.ifs.sge.game.empire.model.map.EmpireProductionState;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.emperor.SimulatedGame;

public class ProductionWorkloadHeuristicValue implements HeuristicValue {

  private final double weight;

  public ProductionWorkloadHeuristicValue(double weight) {
    this.weight = weight;
  }

  @Override
  public double getCoeff() {
    return weight;
  }

  @Override
  public double getHeuristicRatio(SimulatedGame oldState, SimulatedGame newState, int timeMs,
      int playerId) {
    if (newState == null || newState.getInner() == null) {
      return 0;
    }

    return getProducingCities(newState, playerId).size() /
        (double) Math.max(1, newState.getInner().getCitiesByPosition().values().size());
    // (double) Math.max(1, getOccupiedCities(newState, playerId).size());
  }

  private List<EmpireCity> getProducingCities(SimulatedGame game, int playerId) {
    return getOccupiedCities(game, playerId)
        .stream().filter(c -> c.getState() == EmpireProductionState.Producing)
        .collect(Collectors.toList());
  }

  private List<EmpireCity> getOccupiedCities(SimulatedGame game, int playerId) {
    List<EmpireCity> cities = new ArrayList<>();
    for (EmpireCity city : game.getInner().getCitiesByPosition().values()) {
      for (EmpireUnit unit : city.getOccupants()) {
        if (game.getInner().getUnitsByPlayer(playerId).contains(unit)) {
          cities.add(city);
        }
      }
    }
    return cities;
  }

  @Override
  public String toString() {
    return "ProductionWorkLoadHeuristicValue(" + weight + ")";
  }
}
