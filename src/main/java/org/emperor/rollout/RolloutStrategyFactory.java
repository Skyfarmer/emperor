package org.emperor.rollout;

public class RolloutStrategyFactory {

  public RolloutStrategy createRolloutStrategy(RolloutStrategyType type) {
    return switch (type) {
      case explorer -> new MapExplorerStrategy();
      case random -> new RandomRewardStrategy();
      case combined -> new CombinedHeuristicValuesStrategy();
    };
  }
}
