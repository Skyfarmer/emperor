package org.emperor.rollout;

import org.emperor.SimulatedGame;

/**
 * A generic interface for defining custom simulations.
 */
public interface RolloutStrategy {

  /**
   * Runs the simulation.
   *
   * @param game Current simulated game state.
   * @param playerId Player id of the agent.
   * @return A heuristic value between -1 and 1.
   */
  double simulation(SimulatedGame game, int playerId);
}
