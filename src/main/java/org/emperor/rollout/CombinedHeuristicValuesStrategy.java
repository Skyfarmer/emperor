package org.emperor.rollout;

import static org.emperor.EmperorParameters.MAP_EXP_SIMULATION_DURATION_MS;
import static org.emperor.EmperorParameters.MAP_EXP_SIMULATION_PRECISION_MS;

import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.emperor.SimulatedGame;
import org.emperor.macro.GroupedActions;
import org.emperor.rollout.heuristics.CityPresenceHeuristicValue;
import org.emperor.rollout.heuristics.EnemyHealthPointsHeuristicValue;
import org.emperor.rollout.heuristics.ExplorerHeuristicValue;
import org.emperor.rollout.heuristics.HealthPointsHeuristicValue;
import org.emperor.rollout.heuristics.HeuristicValue;
import org.emperor.rollout.heuristics.KilledEnemiesHeuristicValue;
import org.emperor.rollout.heuristics.OccupiedCitiesHeuristicValue;
import org.emperor.rollout.heuristics.ProductionHeuristicValue;
import org.emperor.rollout.heuristics.ProductionWorkloadHeuristicValue;

/**
 * rollout strategy that is based on various heuristic values specified by the HeuristicValue type.
 * Each heuristic ratio is weighted by its associated coefficient.
 */
public class CombinedHeuristicValuesStrategy implements RolloutStrategy {

  public final static Logger log = LogManager.getLogger(CombinedHeuristicValuesStrategy.class);
  private static final double EXPLORE_WEIGHT = 0.3;
  private static final double HEALTH_POINT_WEIGHT = 0;
  private static final double OCCUPIED_CITIES_WEIGHT = 0;
  private static final double PRODUCTION_WORKLOAD_WEIGHT = 0;
  private static final double ENEMY_HEALTH_POINTS_WEIGHT = 1;
  private static final double KILLED_ENEMY_COUNT_WEIGHT = 1;
  private static final double CITY_PRESENCE_WEIGHT = 0;

  private static final double PRODUCTION_WEIGHT = 1;
  private final List<HeuristicValue> heuristicValues;

  // define all employed heuristics with their respective heuristic weights
  public CombinedHeuristicValuesStrategy() {
    this.heuristicValues = List.of(
        new ExplorerHeuristicValue(EXPLORE_WEIGHT),
        new HealthPointsHeuristicValue(HEALTH_POINT_WEIGHT),
        new OccupiedCitiesHeuristicValue(OCCUPIED_CITIES_WEIGHT),
        new ProductionWorkloadHeuristicValue(PRODUCTION_WORKLOAD_WEIGHT),
        new EnemyHealthPointsHeuristicValue(ENEMY_HEALTH_POINTS_WEIGHT),
        new KilledEnemiesHeuristicValue(KILLED_ENEMY_COUNT_WEIGHT),
        new CityPresenceHeuristicValue(CITY_PRESENCE_WEIGHT),
        new ProductionHeuristicValue(PRODUCTION_WEIGHT));
  }

  private double getHeuristicValue(SimulatedGame oldState, SimulatedGame newState, int timeMs,
      int playerId) {
    return heuristicValues.stream()
        .filter(value -> value.getCoeff() > 0.0)
        .map(value -> {
          var d = value.getCoeff() * value.getHeuristicRatio(oldState, newState, timeMs,
              playerId);
          if (d < -1.0 || d > 1.0) {
            log.warn("Heuristic ratio {} of {} not normalized", d, value);
          } else if (!Double.isFinite(d)) {
            log.warn("Heuristic ratio {} is either NaN or infinite", d);
          }
          return d;
        })
        .reduce(Double::sum)
        .map(value -> value / getSumOfWeights())
        .orElse(0.0);
  }

  private double getSumOfWeights() {
    return heuristicValues.stream().map(HeuristicValue::getCoeff).reduce(Double::sum).orElse(0D);
  }

  @Override
  public double simulation(SimulatedGame game, int playerId) {
    var accumulatedTime = 0;
    SimulatedGame oldState = game.copy();

    while (accumulatedTime < MAP_EXP_SIMULATION_DURATION_MS) {
      var possibleActions = game.getPossibleActions(playerId);
      var selectedAction = GroupedActions.getRandomAction(possibleActions, new ArrayList<>());
      selectedAction.ifPresent(game::addAction);
      accumulatedTime += MAP_EXP_SIMULATION_PRECISION_MS;
      game.advance(MAP_EXP_SIMULATION_PRECISION_MS);
    }

    return getHeuristicValue(oldState, game, accumulatedTime, playerId);
  }
}
