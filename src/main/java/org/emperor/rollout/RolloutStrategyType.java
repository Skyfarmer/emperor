package org.emperor.rollout;

public enum RolloutStrategyType {
  random,
  explorer,
  combined
}
