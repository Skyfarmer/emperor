package org.emperor.model;

import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.emperor.EmperorAgent;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class FogOfWarMap extends TimerTask {

    private static final Logger log = LogManager.getLogger(EmperorAgent.class);
    private final Timer timer = new Timer();
    private final boolean[][] discoveredFields;
    private final int height;
    private final int width;
    private final Empire game;
    private EmperorAgent agent;

    public FogOfWarMap(Empire game, EmperorAgent agent) {
        this.width = game.getBoard().getMapSize().getWidth();
        this.height = game.getBoard().getMapSize().getHeight();
        this.discoveredFields = new boolean[width][height];
        this.agent = agent;
        this.game = game;
        this.startUpdate();
    }

    private void startUpdate() {
        timer.schedule(this, new Date(), 1000);
    }

    public synchronized void setDiscovered(int x, int y) {
        if (x < 0 || x >= width || y < 0 || y >= height)
            return;
        discoveredFields[x][y] = true;
    }

    public synchronized void setDiscovered(EmpireUnit unit, int x, int y) {
        int fov = unit.getFov();
        for (int v = y - fov; v <= y + fov; v++) {
            for (int u = x - fov; u <= x + fov; u++) {
                this.setDiscovered(u, v);
            }
        }
    }

    public synchronized boolean hasVisionFor(int x, int y) {
        if (x < 0 || x >= width || y < 0 || y >= height)
            return true; // do not exhaust edges
        return discoveredFields[x][y];
    }

    public void display() {
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                log.info(discoveredFields[x][y] ? "1" : "0");
            }
            log.info("");
        }
    }

    public double size() {
        return width * height;
    }

    public void cleanup() {
        this.cancel();
        this.timer.cancel();
        this.agent = null;
    }

    @Override
    public void run() {
        //log.info("Map Update");
        for (EmpireUnit unit : game.getUnitsByPlayer(agent.getPlayerId())) {
            this.setDiscovered(unit, unit.getPosition().getX(), unit.getPosition().getY());
        }
    }
}
