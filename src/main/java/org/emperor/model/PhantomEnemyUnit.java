package org.emperor.model;

import at.ac.tuwien.ifs.sge.game.empire.map.EmpireMap;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnit;
import java.util.ArrayList;
import java.util.List;

/**
 * use this class to anticipate the presence of units that were once observed and are now hidden
 * behind the fog of war again
 */
public class PhantomEnemyUnit {

  private Position lastPosition;
  private double timestamp;
  private final double healthPoints;
  private final double tilesPerSecond;
  private final double attackPoints;

  public PhantomEnemyUnit(EmpireUnit unit, double timestamp) {
    this.healthPoints = unit.getHp();
    this.timestamp = timestamp;
    this.lastPosition = unit.getPosition();
    this.tilesPerSecond = unit.getTilesPerSecond();
    this.attackPoints = unit.getHitsPerSecond();
  }

  private PhantomEnemyUnit(PhantomEnemyUnit other) {
    this.healthPoints = other.healthPoints;
    this.timestamp = other.timestamp;
    this.lastPosition = other.lastPosition;
    this.tilesPerSecond = other.tilesPerSecond;
    this.attackPoints = other.attackPoints;
  }

  public void updateLastObservation(double timestamp, Position newPosition) {
    this.lastPosition = newPosition;
    this.timestamp = timestamp;
  }

  public List<Position> getPotentialFields(EmpireMap map, double currentTimeStamp) {
    List<Position> fields = new ArrayList<>(
        map.getMapSize().getWidth() * map.getMapSize().getHeight());
    int distance = (int) (tilesPerSecond * (currentTimeStamp - timestamp) / 1000);
    for (int x = lastPosition.getX() - distance; x <= lastPosition.getX() + distance; x++) {
      for (int y = lastPosition.getY() - distance; y <= lastPosition.getY() + distance; y++) {
        Position pos = new Position(x, y);
        if (map.isInside(pos)) {
          fields.add(pos);
        }
      }
    }
    return fields;
  }

  public double getPotentialStrength(EmpireMap map, double currentTimeStamp) {
    return (healthPoints * attackPoints) / getPotentialFields(map, currentTimeStamp).size();
  }

  public PhantomEnemyUnit copy() {
    return new PhantomEnemyUnit(this);
  }

  public Position getLastPosition() {
    return lastPosition;
  }
}
