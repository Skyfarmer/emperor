package org.emperor.model;

import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnit;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * This data structure is used by the agent to model the potential presence of its enemy. Thereby,
 * each agent that has once been observed is modelled by a so-called phantom. Based on the last
 * position the unit was observed, its velocity and the time that has passed since the last
 * observation, the unit might be present in one of the adjacent tiles that are reachable within the
 * time passed. This probable presence is modelled by the phantoms.
 */
public class PhantomEnemyMap {

  private HashMap<UUID, PhantomEnemyUnit> phantomEnemies;

  public PhantomEnemyMap() {
    this.phantomEnemies = new HashMap<>();
  }

  public void update(Empire game, int enemyId) {
    for (EmpireUnit unit : game.getUnitsByPlayer(enemyId)) {
      PhantomEnemyUnit phantom = phantomEnemies.get(unit.getId());
      if (phantom == null) {
        phantomEnemies.put(unit.getId(),
            new PhantomEnemyUnit(unit, game.getGameClock().getGameTimeMs()));
      } else {
        phantom.updateLastObservation(game.getGameClock().getGameTimeMs(), unit.getPosition());
      }
    }
  }

  public PhantomEnemyMap copy() {
    PhantomEnemyMap copy = new PhantomEnemyMap();
    for (UUID id : this.phantomEnemies.keySet()) {
      copy.phantomEnemies.put(id, this.phantomEnemies.get(id).copy());
    }
    return copy;
  }

  public List<PhantomEnemyUnit> getPhantoms() {
    return this.phantomEnemies.values().stream().toList();
  }

}
