package org.emperor;

import at.ac.tuwien.ifs.sge.core.agent.AbstractRealTimeGameAgent;
import at.ac.tuwien.ifs.sge.core.engine.communication.ActionResult;
import at.ac.tuwien.ifs.sge.core.game.GameConfiguration;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.CombatHitAction;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.UnitAppearedAction;
import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.emperor.books.opening.OpeningBook;
import org.emperor.books.opening.OpeningBookFactory;
import org.emperor.books.opening.OpeningBookStrategy;
import org.emperor.macro.RouteAction;
import org.emperor.macro.Scheduler;
import org.emperor.mcts.MCTSNode;
import org.emperor.rollout.RolloutStrategyFactory;

public class EmperorAgent extends AbstractRealTimeGameAgent<Empire, EmpireEvent> {

  private static final Logger log = LogManager.getLogger(EmperorAgent.class);
  private final ExecutorService pool = Executors.newFixedThreadPool(getMinimumNumberOfThreads());
  private final ConcurrentLinkedDeque<Empire> gameQueue = new ConcurrentLinkedDeque<>();
  private final ConcurrentLinkedDeque<EmpireEvent> enemyActionQueue = new ConcurrentLinkedDeque<>();
  private final Map<UUID, EmpireUnit> discoveredEnemyUnits = new HashMap<>();
  private final RolloutStrategyFactory strategyFactory;
  private final OpeningBookFactory openingBookFactory = new OpeningBookFactory();
  private final List<EmpireEvent> rejectedActions = new ArrayList<>();
  private long amountDiscoveredCities = 0;
  private OpeningBook openingBook;

  public EmperorAgent(int playerId, String playerName, int logLevel) {
    super(Empire.class, playerId, playerName, 0);
    this.strategyFactory = new RolloutStrategyFactory();
  }

  public static void main(String[] args) {
    var playerId = getPlayerIdFromArgs(args);
    var playerName = getPlayerNameFromArgs(args);
    var agent = new EmperorAgent(playerId, playerName, -2);
    agent.start();
  }

  @Override
  public void setup(GameConfiguration gameConfiguration, int numberOfPlayers) {
    super.setup(gameConfiguration, numberOfPlayers);
  }

  private Empire initGame() {
    Empire game = null;
    while (game == null) {
      if (!gameQueue.isEmpty()) {
        game = gameQueue.pop();
      }
    }

    return game;
  }

  @Override
  public void startPlaying() {
    super.startPlaying();

    log.info("Emperor has awakened");
    pool.submit(() -> {
      log.info("Play by book");
      var database = new ActionDatabase();
      this.openingBook = openingBookFactory.createOpeningBook(OpeningBookStrategy.produceAndExplore,
          this, database);
      this.openingBook.start();

      log.info("Play by MCTS");
      var game = this.initGame();
      var scheduler = new Scheduler(database);
      while (true) {
        while (!gameQueue.isEmpty()) {
          game = gameQueue.pop();
          scheduler.schedule(game, e -> this.sendAction(e, System.currentTimeMillis() + 50));
        }
        // SUGGESTION: skip game states that are older than certain number of ms (to be defined in EmperorParameters)
        this.run(game, scheduler);
      }
    });
  }

  private void updateKnownEnemies(Empire game, int enemyId) {
    var enemyUnits = game.getUnitsByPlayer(enemyId);
    if (enemyUnits != null) {
      for (var e : enemyUnits) {
        this.discoveredEnemyUnits.put(e.getId(), new EmpireUnit(e));
      }
    }
  }

  private void handleEnemyActionQueue(Empire game, Scheduler scheduler) {
    while (!this.enemyActionQueue.isEmpty()) {
      var p = this.enemyActionQueue.pop();
      if (p instanceof UnitAppearedAction) {
        log.debug("Enemy appeared, clearing all movement actions");
        scheduler.cancelAll(RouteAction.class);
      }
    }
  }

  private void run(Empire game, Scheduler scheduler) {
    try {
      if (game.getCitiesByPosition().size() > this.amountDiscoveredCities) {
        // New city discovered, clear all scheduled actions to adjust to new situation
        log.debug("New city discovered, removing all actions from schedule");
        this.amountDiscoveredCities = game.getCitiesByPosition().size();
        scheduler.cancelAll(RouteAction.class);
      }
      updateKnownEnemies(game, (playerId + 1) % 2);
      handleEnemyActionQueue(game, scheduler);

      var g = new SimulatedGame(game, scheduler.copy(), playerId);
      var tree = new MCTSNode(this.playerId, g, null);
      var bestNodes = tree.utcSearch();
      var treeDepth = tree.treeDepth();
      if (treeDepth != 1) {
        log.debug("Tree Depth: {}", treeDepth);
      }
      assert treeDepth != 1 || bestNodes.isEmpty();

      if (!bestNodes.isEmpty()) {
        for (var a : bestNodes) {
          var resp_action = a.getResponsibleAction();
          log.info("Sending macro action {} with Q-value {}", resp_action, a.getHeuristicValue());
          scheduler.add(resp_action);
          scheduler.schedule(game, e -> this.sendAction(e, System.currentTimeMillis() + 50));
        }
      }
    } catch (Exception e) {
      log.error("Exception occurred:", e);
    }
  }

  @Override
  public void sendAction(EmpireEvent action, long executionTimeMs) {
    log.trace("Sending micro action {}", action);
    super.sendAction(action, executionTimeMs);
  }

  @Override
  protected void onGameUpdate(EmpireEvent action, ActionResult result) {
    this.gameQueue.add((Empire) this.game.copy());
    if (this.openingBook != null) {
      this.openingBook.onGameUpdate(action, result);
    }
    if (action instanceof UnitAppearedAction v) {
      this.enemyActionQueue.add(v);
    } else if (action instanceof CombatHitAction att) {
      if (game.getUnit(att.getTargetId()) == null) {
        this.discoveredEnemyUnits.remove(att.getTargetId());
      }
    }
  }

  public int getPlayerId() {
    return this.playerId;
  }

  public Empire getGame() {
    return game;
  }

  @Override
  protected void onActionRejected(EmpireEvent action) {
    log.debug("Rejected action: {}", action);
    this.rejectedActions.add(action);
  }

  @Override
  public void shutdown() {

  }

  @Override
  protected int getMinimumNumberOfThreads() {
    return super.getMinimumNumberOfThreads() + EmperorParameters.NUMBER_OF_THREADS;
  }

  public List<EmpireUnit> getAvailableUnits(ActionDatabase db) {
    return game.getUnitsByPlayer(playerId).stream().filter(u -> db.isAvailable(u.getId())).toList();
  }

  public void releaseOpeningBook() {
    this.openingBook = null;
  }
}
