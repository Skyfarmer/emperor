package org.emperor;

import static org.emperor.Utilities.isValidTile;

import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.jetbrains.annotations.NotNull;

public class Astar {

  private Astar() {
  }

  /**
   * A-Star algorithm based on the positions of an empire map.
   *
   * @param start Start position.
   * @param destination Destination.
   * @param game Current game state.
   * @param playerId Player id of the agent.
   * @param estimate Heuristic function to estimate the distance between two positions. Should be a
   * lower bound for optimal shortest paths.
   * @return A list of positions, describing the path from start to destination or null, in case no
   * route could be found.
   */
  public static List<Position> astar(Position start,
      Position destination, Empire game, int playerId, Function<Position, Long> estimate) {
    var visit = new PriorityQueue<PositionWithHeuristic>();
    // The actual cost so far (g-values)
    Map<Position, Long> costs = new HashMap<>();
    // Predecessor list
    Map<Position, Position> pred = new HashMap<>();

    costs.put(start, 0L);
    visit.add(new PositionWithHeuristic(start, estimate.apply(start)));
    while (!visit.isEmpty()) {
      var next = visit.poll();
      if (next.destination.equals(destination)) {
        // Destination reached, traverse shortest path.
        List<Position> path = new ArrayList<>();
        var current = next.destination;
        while (current != null) {
          path.add(current);
          current = pred.get(current);
        }
        Collections.reverse(path);
        return path;
      }
      // We use a stricter tile validator for the initial move
      var neighbors = next.destination.equals(start) ? next.getNeighbors(game,
          p -> isValidTile(game, p, playerId)) : next.getNeighbors(game, p -> isValidTile(game, p));

      for (var n : neighbors) {
        var tmpCosts = costs.get(next.destination) + 1;
        var currentCosts = costs.get(n);
        if (currentCosts == null || tmpCosts < currentCosts) {
          costs.put(n, tmpCosts);
          pred.put(n, next.destination);
          visit.add(new PositionWithHeuristic(n, tmpCosts + estimate.apply(n)));
        }
      }
    }

    return null;
  }

  /**
   * A position which also stores the distance from the start node.
   */
  private record PositionWithHeuristic(Position destination, long distance) implements
      Comparable<PositionWithHeuristic> {

    /**
     * Returns all valid neighboring tiles.
     * @param game Current game state.
     * @param validator A validation function to determine whether this tile can be walked upon.
     * @return A list of all valid neighboring tiles.
     */
    List<Position> getNeighbors(Empire game, Function<Position, Boolean> validator) {
      return Stream.of(destination.getLeftUpper(), destination.getUpper(),
              destination.getRightUpper(), destination.getLeft(), destination.getRight(),
              destination.getLeftLower(), destination.getLower(), destination.getRightLower())
          .filter(p -> game.getBoard().isInside(p) && validator.apply(p))
          .collect(Collectors.toList());
    }

    @Override
    public int compareTo(@NotNull PositionWithHeuristic o) {
      return Long.compare(this.distance, o.distance);
    }
  }
}
