package org.emperor.macro;

import static org.emperor.Utilities.isUnitAvailable;

import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;
import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import org.emperor.ActionDatabase;
import org.emperor.Utilities.Either;
import org.emperor.Utilities.UnitTypes;

/**
 * Occupy a city and produce a unit.
 */
public class OccupyAndProduceAction implements MacroAction {

  private final UUID unit;
  private final Position city;
  private final UnitTypes unitType;
  private Either<RouteAction, ProductionAction> subAction;

  OccupyAndProduceAction(Position city, UnitTypes type, UUID unit) {
    this.unit = unit;
    this.city = city;
    this.unitType = type;
    this.subAction = Either.left(new RouteAction(unit, city));
  }

  private OccupyAndProduceAction(OccupyAndProduceAction other) {
    this.unit = other.unit;
    this.city = other.city;
    this.unitType = other.unitType;
    this.subAction = other.subAction.map(l -> (RouteAction) l.copy(),
        r -> (ProductionAction) r.copy());
  }

  /**
   * Construct all valid OccupyAndProduceActions based on the current game state and available units
   * and cities.
   * <p>
   * The result is the cross product of all unoccupied cities, available player units and unit types
   * that can be produced.
   *
   * @param game Current game state.
   * @param db Action database to determine available units and cities.
   * @param playerId The player id of the agent.
   * @return A list of all valid OccupyAndProduceActions.
   */
  public static List<GroupedActions> getPossibleActions(Empire game, ActionDatabase db,
      int playerId) {
    if (game.getUnitsByPlayer(playerId).size() > 10) {
      return List.of();
    }
    var availableCities = game.getCitiesByPosition().values().stream()
        // Only consider unoccupied cities
        .filter(c -> c.getPlayerId() == -1)
        .filter(c -> db.isAvailable(c.getPosition()));

    var availableUnits = game.getUnitsByPlayer(playerId).stream()
        .filter(u -> isUnitAvailable(game, u.getId(), db)).toList();

    return availableCities.flatMap(c -> availableUnits.stream().map(
            u -> Arrays.stream(UnitTypes.values())
                .map(t -> new OccupyAndProduceAction(c.getPosition(), t, u.getId()))
                .map(t -> (MacroAction) t)))
        .map(r -> r.collect(Collectors.toList()))
        .filter(r -> !r.isEmpty())
        .map(GroupedActions::new)
        .toList();
  }

  @Override
  public String toString() {
    return String.format("Occupy and %s", this.subAction);
  }

  private boolean isCityOccupiedByPlayer(Empire game, Position city, int playerId) {
    return game.getCity(city).getPlayerId() == playerId;
  }

  @Override
  public boolean isComplete(Empire game) {
    if (this.subAction.isLeft()) {
      var routeAction = this.subAction.getLeft();
      var unit = game.getUnit(routeAction.getUnitId());
      if (unit == null) {
        // Unit is dead
        return true;
      }
      var enemyId = (unit.getPlayerId() + 1) % 2;
      if (isCityOccupiedByPlayer(game, routeAction.getDestination(), enemyId)) {
        // City is occupied by enemy, cancel operation
        return true;
      }
      if (unit.getPosition().equals(routeAction.getDestination())) {
        // Unit has reached city, start production
        this.subAction = Either.right(
            new ProductionAction(routeAction.getDestination(), unitType, unit.getId()));
      }
      return false;
    } else {
      return this.subAction.getRight().isComplete(game);
    }
  }

  @Override
  public boolean canProgress(Empire game) {
    return this.subAction.reduce(l -> l.canProgress(game), r -> r.canProgress(game));
  }

  @Override
  public List<EmpireEvent> apply(Empire game) {
    return this.subAction.reduce(l -> l.apply(game), r -> r.apply(game));
  }

  @Override
  public Optional<Position> affectedCity() {
    return Optional.of(this.city);
  }

  @Override
  public UUID affectedUnit() {
    return this.unit;
  }

  @Override
  public MacroAction copy() {
    return new OccupyAndProduceAction(this);
  }
}
