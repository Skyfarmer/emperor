package org.emperor.macro;

import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Deque;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Class to group actions together.
 */
public class GroupedActions {

  private final Deque<MacroAction> actions;

  public GroupedActions(List<MacroAction> actions) {
    if (actions.isEmpty()) {
      throw new RuntimeException("Initial actions must not be empty");
    }
    Collections.shuffle(actions);
    this.actions = new ArrayDeque<>(actions);
  }

  /**
   * Extract and remove a random action from a random group.
   *
   * @param groups: List of groups.
   * @param avoidUnit A list of units that should be avoided if possible. The idea here is to prefer
   * actions associated with units that have not yet received an action at all.
   * @return The selected actions. Returns empty if input is empty.
   */
  public static Optional<MacroAction> getRandomAction(List<GroupedActions> groups,
      List<UUID> avoidUnit) {
    if (groups.isEmpty()) {
      return Optional.empty();
    }
    var group = groups.stream()
        .filter(g -> !avoidUnit.contains(g.peek().affectedUnit()))
        .toList();
    if (group.isEmpty()) {
      group = groups;
    }
    var i = ThreadLocalRandom.current().nextInt(group.size());
    var g = group.get(i);

    var e = g.pop();
    if (g.isEmpty()) {
      groups.remove(g);
    }
    return Optional.of(e);
  }

  /**
   * Look at the first action in the queue.
   *
   * @return Returns the first action in the queue.
   */
  public MacroAction peek() {
    return actions.peek();
  }

  /**
   * @return Removes and returns the first action in the list.
   */
  public MacroAction pop() {
    return actions.pop();
  }

  public boolean isEmpty() {
    return actions.isEmpty();
  }
}
