package org.emperor.macro;

import static org.emperor.Utilities.isAdjacent;
import static org.emperor.Utilities.isUnitOnTop;

import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start.CombatStartOrder;
import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import java.util.List;
import java.util.UUID;

/**
 * This action handles attacking an enemy unit in proximity. It is invalid once the target unit is
 * out of range.
 */
public class AttackAction implements MacroAction {

  private final UUID enemy;
  private final UUID unit;
  private boolean hasStarted;

  public AttackAction(UUID unit, UUID enemy) {
    this.unit = unit;
    this.enemy = enemy;
    this.hasStarted = false;
  }

  public AttackAction(AttackAction other) {
    this.enemy = other.enemy;
    this.unit = other.unit;
    this.hasStarted = other.hasStarted;
  }

  @Override
  public boolean isComplete(Empire game) {
    var unit = game.getUnit(this.unit);
    var enemy = game.getUnit(this.enemy);

    // The action is complete if
    // * the attacker is dead
    // * the enemy unit is dead
    // * the enemy unit is not in range
    return unit == null || enemy == null || !isAdjacent(unit.getPosition(), enemy.getPosition());
  }

  @Override
  public boolean canProgress(Empire game) {
    var unit = game.getUnit(this.unit);
    return unit.isIdle();
  }

  /**
   * Helper function to determine whether the attack action is currently valid. This involves
   * additional constraints such as whether the attacker or enemy unit are the topmost one in case
   * of a city.
   */
  public boolean isValid(Empire game) {
    var unit = game.getUnit(this.unit);
    if (unit == null) {
      return false;
    }

    var enemy = game.getUnit(this.enemy);
    return enemy != null && isAdjacent(unit.getPosition(), enemy.getPosition())
        && isUnitOnTop(unit.getId(), game) && isUnitOnTop(enemy.getId(), game);
  }

  @Override
  public List<EmpireEvent> apply(Empire game) {
    this.hasStarted = true;
    return List.of(new CombatStartOrder(unit, enemy));
  }

  @Override
  public UUID affectedUnit() {
    return this.unit;
  }

  @Override
  public MacroAction copy() {
    return new AttackAction(this);
  }
}
