package org.emperor.macro;

import static org.emperor.Utilities.isAdjacent;
import static org.emperor.Utilities.isUnitOnTop;
import static org.emperor.Utilities.isValidTile;

import at.ac.tuwien.ifs.sge.core.util.pair.ImmutablePair;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;
import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import org.emperor.ActionDatabase;
import org.emperor.Utilities.Either;

/**
 * Attacks a unit that is not in close distance.
 */
public class MoveAndAttackAction implements MacroAction {

  private final UUID unit;
  private final UUID enemy;
  private Either<RouteAction, AttackAction> subAction;

  public MoveAndAttackAction(UUID unit, UUID enemy) {
    this.unit = unit;
    this.enemy = enemy;
    this.subAction = null;
  }

  public MoveAndAttackAction(MoveAndAttackAction other) {
    this.unit = other.unit;
    this.enemy = other.enemy;
    if (other.subAction == null) {
      this.subAction = null;
    } else {
      other.subAction.map(r -> (RouteAction) r.copy(), a -> (AttackAction) a.copy());
    }
  }

  /**
   * Determine the available actions in the current game state as well as occupied units/cities. The
   * result is the cross product between all available player's as well as enemy units.
   *
   * @param game The current game.
   * @param db Action database to determine what units and cities are available.
   * @param playerId The player id of the agent.
   * @return A list of all valid MoveAndAttack actions.
   */
  public static Collection<GroupedActions> getPossibleActions(Empire game, ActionDatabase db,
      int playerId) {
    var enemyId = (playerId + 1) % 2;
    var enemyUnits = game.getUnitsByPlayer(enemyId);

    return game.getUnitsByPlayer(playerId).stream()
        .filter(u -> db.isAvailable(u.getId()))
        .map(u -> enemyUnits.stream()
            .map(e -> new MoveAndAttackAction(u.getId(), e.getId()))
            .map(t -> (MacroAction) t)
            .collect(Collectors.toList()))
        .filter(l -> !l.isEmpty())
        .map(GroupedActions::new)
        .toList();
  }

  @Override
  public boolean isComplete(Empire game) {
    var enemyUnit = game.getUnit(enemy);
    // Action is complete if enemy does not exist (not visible) or is dead
    // If unit is not on top, we can't attack it
    // Just ignore it then and conclude the action.
    return enemyUnit == null || !enemyUnit.isAlive() || !isUnitOnTop(this.enemy, game);
  }

  /**
   * Find the closest valid tile that the enemy can be attacked from.
   *
   * @param game Current game state.
   * @return The closest enemy tile if available.
   */
  private Optional<Position> findCloseEnemyTile(Empire game) {
    var unit = game.getUnit(this.unit);
    var enemy = game.getUnit(this.enemy).getPosition();
    var playerId = unit.getPlayerId();
    var minPos = enemy.getAllNeighbours().stream()
        // Ignore current position
        .filter(p -> !p.equals(unit.getPosition()))
        // Ignore invalid tiles
        .filter(p -> isValidTile(game, p, playerId))
        .map(p -> {
          var x = Math.abs(unit.getPosition().getX() - p.getX());
          var y = Math.abs(unit.getPosition().getY() - p.getY());
          return new ImmutablePair<>(p, x + y);
        }).min(Comparator.comparingInt(ImmutablePair::getB));

    return minPos.map(ImmutablePair::getA);
  }

  /**
   * Determine the next required action. If the enemy can be attacked, choose the AttackAction.
   * Otherwise, we apply a RouteAction to reach the enemy.
   *
   * @param game Current game state.
   * @return The appropriate subsequent action.
   */
  private Either<RouteAction, AttackAction> chooseAction(Empire game) {
    var unit = game.getUnit(this.unit);
    var enemy = game.getUnit(this.enemy);
    if (isAdjacent(unit.getPosition(), enemy.getPosition())) {
      if (isUnitOnTop(this.unit, game)) {
        // The enemy is nearby, fight it
        return Either.right(new AttackAction(this.unit, this.enemy));
      } else {
        // Unit is not on top and thus, cannot fight.
        // Move to another tile.
        // In case no tile is available, don't move.
        var newPos = this.findCloseEnemyTile(game).orElse(unit.getPosition());
        return Either.left(new RouteAction(this.unit, newPos));
      }
    } else {
      // Move towards the enemy
      return Either.left(new RouteAction(this.unit, enemy.getPosition()));
    }
  }

  @Override
  public boolean canProgress(Empire game) {
    var unit = game.getUnit(this.unit);

    if (unit == null) {
      // Unit is dead
      return false;
    }

    if (subAction == null) {
      this.subAction = this.chooseAction(game);
    } else if (subAction.isLeft()) {
      var routeAction = subAction.getLeft();
      var destination = routeAction.getDestination();

      var enemyUnit = game.getUnit(enemy);
      if (isAdjacent(unit.getPosition(), enemyUnit.getPosition())) {
        // The enemy is in range, attack
        this.subAction = this.chooseAction(game);
      } else if (!destination.equals(enemyUnit.getPosition())) {
        // Position of enemy has changed, adapt course
        this.subAction = this.chooseAction(game);
      }
    } else {
      var attackAction = subAction.getRight();
      if (!attackAction.isValid(game)) {
        // The attack action is not valid anymore, e.g. because the enemy moved
        this.subAction = this.chooseAction(game);
      }
    }

    return this.subAction.reduce(l -> l.canProgress(game), r -> r.canProgress(game));
  }

  @Override
  public List<EmpireEvent> apply(Empire game) {
    return subAction.reduce(l -> l.apply(game), r -> r.apply(game));
  }

  @Override
  public UUID affectedUnit() {
    return this.unit;
  }

  @Override
  public MacroAction copy() {
    return new MoveAndAttackAction(this);
  }

  @Override
  public String toString() {
    return String.format("Move %s and attack %s", this.unit, this.enemy);
  }
}
