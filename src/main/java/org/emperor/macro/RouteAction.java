package org.emperor.macro;

import static org.emperor.Astar.astar;
import static org.emperor.Utilities.isUnitAvailable;
import static org.emperor.Utilities.isValidTile;

import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start.MovementStartOrder;
import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnitState;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.emperor.ActionDatabase;

/**
 * This action is responsible for moving a unit to an arbitrary position on the map.
 */
public class RouteAction implements MacroAction {

  private static final Logger log = LogManager.getLogger(RouteAction.class);
  private final UUID unitId;
  private final Position destination;

  RouteAction(UUID unitId, Position destination) {
    this.unitId = unitId;
    this.destination = destination;
  }

  private RouteAction(RouteAction other) {
    this.unitId = other.unitId;
    this.destination = other.destination;
  }

  /**
   * Produces all valid RouteAction moves. The result is the cross product between all available
   * player units and possible destination (e.g. not a mountain or enemy city).
   *
   * @param game Current game state.
   * @param db Action database to determine all available units.
   * @param playerId The player id of the agent.
   * @return A list of all legal unit moves.
   */
  public static List<GroupedActions> getPossibleActions(Empire game, ActionDatabase db,
      int playerId) {
    var freeUnits = game.getUnitsByPlayer(playerId)
        .stream()
        .filter(u -> isUnitAvailable(game, u.getId(), db));

    return freeUnits.map(u -> {
      var width = game.getBoard().getMapSize().getWidth();
      var height = game.getBoard().getMapSize().getHeight();

      List<MacroAction> actions = new ArrayList<>();
      for (var i = 0; i < width; i++) {
        for (var j = 0; j < height; j++) {
          var pos = new Position(i, j);
          if (!u.getPosition().equals(pos) && isValidTile(game, pos, playerId)) {
            actions.add(new RouteAction(u.getId(), pos));
          }
        }
      }
      return new GroupedActions(actions);
    }).toList();
  }

  /**
   * Calculates the shortest path between two positions using A-star and the euclidean distance as a
   * heuristic.
   *
   * @param game Current game state.
   * @param playerId Player id of the agent.
   * @param start Start position.
   * @param end Destination.
   * @return List of positions to reach destination or null, in case no path could be found.
   */
  public static List<Position> getShortestPath(Empire game, int playerId, Position start,
      Position end) {
    return astar(start, end, game, playerId, p -> {
      // Pythagorean Theorem
      var cX = p.getX();
      var cY = p.getY();
      var dX = end.getX();
      var dY = end.getY();

      var deltaX = Math.abs(dX - cX);
      var deltaY = Math.abs(dY - cY);

      return (long) Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));
    });
  }

  @Override
  public String toString() {
    return String.format("Walk to %s (Unit: %s)", this.destination, this.unitId);
  }

  @Override
  public UUID affectedUnit() {
    return this.unitId;
  }

  @Override
  public MacroAction copy() {
    return new RouteAction(this);
  }

  public List<EmpireEvent> apply(Empire game) {
    var unit = game.getUnit(this.unitId);
    var route = getShortestPath(game, unit.getPlayerId(), unit.getPosition(), this.destination);

    if (route == null) {
      log.warn("No route from {} to {}", unit.getPosition(), this.destination);
      return List.of();
    }

    if (route.size() >= 2) {
      // The first element corresponds to the current location of the unit
      // Thus, we retrieve the second element as its next intermediate destination
      var successor = route.get(1);
      return List.of(new MovementStartOrder(this.unitId, successor));
    } else {
      // Already at destination.
      return List.of();
    }
  }

  public boolean canProgress(Empire game) {
    var unit = game.getUnit(unitId);
    return unit.getState() == EmpireUnitState.Idle;
  }

  private boolean routeExists(Empire game) {
    var route = this.getRoute(game);
    return route != null;
  }

  Position getDestination() {
    return destination;
  }

  UUID getUnitId() {
    return unitId;
  }

  private List<Position> getRoute(Empire game) {
    var unit = game.getUnit(this.unitId);
    return getShortestPath(game, unit.getPlayerId(), unit.getPosition(), this.destination);
  }

  public boolean isComplete(Empire game) {
    var unit = game.getUnit(unitId);
    // Action is complete if unit is dead, has reached its destination or cannot reach its
    // destination.
    return unit == null || unit.getPosition().equals(destination) || !this.routeExists(game);
  }
}
