package org.emperor.macro;

import static org.emperor.Utilities.isUnitAvailable;

import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start.ProductionStartOrder;
import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import at.ac.tuwien.ifs.sge.game.empire.model.map.EmpireProductionState;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import org.emperor.ActionDatabase;
import org.emperor.Utilities.UnitTypes;

/**
 * This action produces a certain type of unit and ensures that a unit is responsible for staying at
 * a city until the unit has been produced.
 */
public class ProductionAction implements MacroAction {

  private final Position city;
  private final UnitTypes unitType;
  private final UUID responsibleUnit;
  private long startTime = 0;

  ProductionAction(Position city, UnitTypes unitType, UUID responsibleUnit) {
    this.city = city;
    this.unitType = unitType;
    this.responsibleUnit = responsibleUnit;
  }

  private ProductionAction(ProductionAction other) {
    this.city = other.city;
    this.unitType = other.unitType;
    this.responsibleUnit = other.responsibleUnit;
    this.startTime = other.startTime;
  }

  /**
   * Returns all valid ProductionActions. For each occupied city, the result is the cross product
   * between the occupying units and all unit types (cavalry, scout and infantry).
   *
   * @param game Current game state.
   * @param db Action database to determine all available units.
   * @param playerId The player id of the agent.
   * @return A list of all legal ProductionActions.
   */
  public static List<GroupedActions> getPossibleActions(Empire game, ActionDatabase db,
      int playerId) {
    if (game.getUnitsByPlayer(playerId).size() > 10) {
      return List.of();
    }
    var availableCities = game.getCitiesByPosition().values().stream()
        // The city must be occupied by us
        .filter(c -> c.getPlayerId() == playerId)
        // We only consider idle cities
        .filter(c -> db.isAvailable(c.getPosition()));

    return availableCities.map(
            c -> c.getOccupants().stream()
                .filter(u -> isUnitAvailable(game, u.getId(), db))
                .flatMap(
                    u -> Arrays.stream(UnitTypes.values())
                        .map(t -> new ProductionAction(c.getPosition(), t, u.getId()))
                        .map(t -> (MacroAction) t)))
        .map(l -> l.collect(Collectors.toList()))
        .filter(v -> !v.isEmpty())
        .map(GroupedActions::new)
        .toList();
  }

  /**
   * Calculates the ingame time required to produce a certain type. Returns in ms.
   *
   * @param type The unit type.
   * @return The amount of in-game time required in milliseconds.
   */
  private static long timeNeededFor(UnitTypes type) {
    return switch (type) {
      case Infantry, Scout -> 10;
      case Cavalry -> 20;
    } * 1000;
  }

  @Override
  public boolean isComplete(Empire game) {
    if (this.startTime == 0) {
      // Not yet started
      return false;
    } else if (game.getUnit(this.responsibleUnit) == null) {
      // Unit has probably been killed
      return true;
    } else if (!game.getUnit(this.responsibleUnit).getPosition().equals(this.city)) {
      // This should actually not happen but we want to make sure. In case, the unit is not
      // on the city field.
      return true;
    }

    var timeNeeded = timeNeededFor(this.unitType);
    var currentTime = game.getGameClock().getGameTimeMs();
    var city = game.getCity(this.city);
    // If city is idle and sufficient time has passed, the unit should be completed by then.
    return city.getState() == EmpireProductionState.Idle
        && currentTime - this.startTime > timeNeeded;
  }

  @Override
  public String toString() {
    return String.format("Produce %s at %s", unitType.toString(), this.city);
  }

  @Override
  public boolean canProgress(Empire game) {
    return this.startTime == 0;
  }

  @Override
  public List<EmpireEvent> apply(Empire game) {
    this.startTime = game.getGameClock().getGameTimeMs();
    return List.of(new ProductionStartOrder(this.city, this.unitType.getValue()));
  }

  @Override
  public UUID affectedUnit() {
    return this.responsibleUnit;
  }

  @Override
  public MacroAction copy() {
    return new ProductionAction(this);
  }

  @Override
  public Optional<Position> affectedCity() {
    return Optional.of(this.city);
  }
}
