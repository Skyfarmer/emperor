package org.emperor.macro;

import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;
import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.emperor.ActionDatabase;

/**
 * The scheduler applies micro action from given macro actions.
 */
public class Scheduler {

  private static final Logger log = LogManager.getLogger(Scheduler.class);
  private final List<MacroAction> scheduledActions;
  private final ActionDatabase database;

  public Scheduler(ActionDatabase db) {
    this.scheduledActions = new ArrayList<>();
    this.database = db;
  }

  private Scheduler(Scheduler other) {
    this.scheduledActions = other.scheduledActions.stream().map(MacroAction::copy)
        .collect(Collectors.toList());
    this.database = other.database.copy();
  }

  /**
   * Returns the internal action database. Any modifications to the database are undefined
   * behaviour.
   *
   * @return The internal action database.
   */
  public ActionDatabase getDatabase() {
    return database;
  }

  /**
   * Schedule a new macro action.
   *
   * @param action The action to schedule.
   */
  public void add(MacroAction action) {
    log.trace("Scheduling action: {}", action);
    this.database.add(action.affectedUnit());
    action.affectedCity().ifPresent(this.database::add);
    scheduledActions.add(action);
  }

  /**
   * Unregisters the given action from the internal database.
   *
   * @param action The action to unregister from the database.
   */
  private void remove(MacroAction action) {
    this.database.remove(action.affectedUnit());
    action.affectedCity().ifPresent(this.database::remove);
  }

  /**
   * Removes all action of the given type.
   *
   * @param type The class type to remove from the scheduler.
   */
  public void cancelAll(Class<? extends MacroAction> type) {
    var toBeRemoved = this.scheduledActions
        .stream().filter(type::isInstance).toList();
    for (var a : toBeRemoved) {
      this.remove(a);
    }
    this.scheduledActions.removeAll(toBeRemoved);
  }

  /**
   * Create a deep copy of this instance.
   *
   * @return A fresh copy of the scheduler.
   */
  public Scheduler copy() {
    return new Scheduler(this);
  }

  /**
   * Apply new actions to the current state.
   *
   * @param game The current game state.
   * @param app A function to handle the application of a new micro action. The function will be
   * called for each micro action retrieved.
   */
  public void schedule(Empire game, Consumer<EmpireEvent> app) {
    // Remove if action is now complete
    scheduledActions.removeIf(a -> {
      if (a.isComplete(game)) {
        log.trace("Unregistering {}", a);
        this.remove(a);
        return true;
      } else {
        return false;
      }
    });

    for (var action : scheduledActions) {
      if (action.canProgress(game)) {
        var event = action.apply(game);
        // TODO Handle multiple events
        if (!event.isEmpty()) {
          app.accept(event.get(0));
        }
      }
    }
  }
}
