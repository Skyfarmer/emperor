package org.emperor.macro;

import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;
import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.emperor.ActionDatabase;

/**
 * This interface defines all necessary functions to define custom macro actions.
 */
public interface MacroAction {

  /**
   * Computes all possible macro actions currently provided by the agent.
   *
   * @param game: Current game state.
   * @param db: Action database to determine available units/cities.
   * @param playerId: The player id of the agent.
   * @return List of all legal actions.
   */
  static List<GroupedActions> getPossibleActions(Empire game, ActionDatabase db, int playerId) {
    List<GroupedActions> actions = new ArrayList<>(
        RouteAction.getPossibleActions(game, db, playerId));
    actions.addAll(ProductionAction.getPossibleActions(game, db, playerId));
    actions.addAll(OccupyAndProduceAction.getPossibleActions(game, db, playerId));
    actions.addAll(MoveAndAttackAction.getPossibleActions(game, db, playerId));

    return actions;
  }

  /**
   * Tells the scheduler whether the action is complete independently from whether the action was a
   * success or cannot be applied anymore.
   *
   * @param game: Current game state.
   * @return Whether the action is finished.
   */
  boolean isComplete(Empire game);

  /**
   * Return whether the action can apply a new micro action to the current game state.
   *
   * @param game: Current game state.
   * @return Return whether a new micro action can be computed.
   */
  boolean canProgress(Empire game);

  /**
   * Return the next micro Action. This function will be called after the action notifies the
   * scheduler via canProgress that a new micro action is applicable. This function computes that
   * micro action.
   *
   * @param game: The current game state.
   * @return Return a list of micro actions to be applied. However, the scheduler currently only
   * applies the first action in the list.
   */
  List<EmpireEvent> apply(Empire game);

  /**
   * Returns the unit associated with the action.
   *
   * @return The affected unit.
   */
  UUID affectedUnit();

  /**
   * Returns whether the associated action applies to a city in the game (e.g. Unit production).
   *
   * @return The affected city.
   */
  default Optional<Position> affectedCity() {
    return Optional.empty();
  }

  /**
   * Copies the given MacroAction.
   *
   * @return A deep copy of itself.
   */
  MacroAction copy();
}
