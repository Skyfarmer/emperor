package org.emperor;

import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireMapException;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import at.ac.tuwien.ifs.sge.game.empire.model.map.EmpireTerrain;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnit;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnitState;
import java.util.UUID;
import java.util.function.Function;
import java.util.function.Supplier;

public class Utilities {

  private Utilities() {
  }

  /**
   * We noticed that the game sometimes incorrectly reports a unit as fighting. This function checks
   * whether the fight can actually happen.
   *
   * @param game Current game state.
   * @param unit UUID of the `fighting` unit.
   * @return True, if fight is legit. False, otherwise.
   */
  private static boolean isLegitFight(Empire game, EmpireUnit unit) {
    if (unit.getState() == EmpireUnitState.Fighting) {
      var target = game.getUnit(unit.getTargetId());
      return target != null && isAdjacent(unit.getPosition(), target.getPosition());
    }
    return false;
  }

  /**
   * Checks whether the unit is available.
   *
   * @param game Current game state.
   * @param id UUID of the target unit.
   * @param db Action database.
   * @return True, if unit is available.
   */
  public static boolean isUnitAvailable(Empire game, UUID id, ActionDatabase db) {
    var unit = game.getUnit(id);
    return unit != null
        && db.isAvailable(id)
        && (unit.getState() == EmpireUnitState.Idle || !isLegitFight(game, unit));
  }

  /**
   * Checks whether tile is walkable for the given player. A tile is valid, if it is covered in fog
   * of war, has not reached its maximum number of units and is occupied by the player itself.
   *
   * @param game Current game state.
   * @param destination Tile to check for.
   * @param playerId Player id to check for.
   * @return True, if tile is valid.
   */
  public static boolean isValidTile(Empire game, Position destination, int playerId) {
    if (!game.getBoard().isInside(destination)) {
      return false;
    }
    try {
      var tile = game.getBoard().getTile(destination);
      if (tile == null) {
        return true;
      }
      var isOccupiable =
          tile.getPlayerId() == playerId || tile.getPlayerId() == EmpireTerrain.UNOCCUPIED_ID;
      var occupants = tile.getOccupants();
      return occupants != null && occupants.size() != tile.getMaxOccupants() && isOccupiable;
    } catch (EmpireMapException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Checks whether two position are adjacent to each other.
   *
   * @param f First tile.
   * @param s Second tile.
   * @return True if both tiles are adjacent.
   */
  public static boolean isAdjacent(Position f, Position s) {
    var x = Math.abs(f.getX() - s.getX());
    var y = Math.abs(f.getY() - s.getY());
    return x <= 1 && y <= 1;
  }

  /**
   * Checks whether the given unit is topmost on its tile.
   *
   * @param id UUID of the unit.
   * @param game Current game state.
   * @return True, if unit is the topmost on its tile.
   */
  public static boolean isUnitOnTop(UUID id, Empire game) {
    var unit = game.getUnit(id);
    try {
      var tile = game.getBoard().getTile(unit.getPosition());
      return tile.getOccupants().peek().getId().equals(id);
    } catch (EmpireMapException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Checks whether the tile can be walked upon. This function checks whether the tile has not
   * reached its maximum occupants independently of which player occupies the tile. A tile is also
   * valid if it is covered by fog of war (and thus, null).
   *
   * @param game Current game state.
   * @param destination Tile to check
   * @return True, if tile is valid.
   */
  public static boolean isValidTile(Empire game, Position destination) {
    try {
      var tile = game.getBoard().getTile(destination);
      if (tile == null) {
        return true;
      }
      return tile.getMaxOccupants() != 0;
    } catch (EmpireMapException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Internal function for defining unit types.
   */
  public enum UnitTypes {
    Infantry(1),
    Scout(2),
    Cavalry(3);

    private final int value;

    UnitTypes(int value) {
      this.value = value;
    }

    public static UnitTypes fromUnitTypeId(int id) {
      return switch (id) {
        case 1 -> Infantry;
        case 2 -> Scout;
        case 3 -> Cavalry;
        default -> throw new RuntimeException("Unknown unit with id " + id);
      };
    }

    /**
     * Retrieve the id of the unit type used by the Empire game.
     *
     * @return Returns the id.
     */
    public int getValue() {
      return value;
    }

  }

  /**
   * The Either class is a common pattern in function program. It allows to store two different
   * types but only one of them at the same time.
   *
   * @param <L> Type of the left value.
   * @param <R> Type of the right value.
   */
  public static class Either<L, R> {

    private final L left;
    private final R right;

    private Either(L left, R right) {
      this.left = left;
      this.right = right;
    }

    /**
     * Constructs a new left instance using the given type.
     *
     * @param left The left value to store.
     * @param <L> The type of left.
     * @param <R> The type of right.
     * @return Returns a new instance of Either.
     */
    public static <L, R> Either<L, R> left(L left) {
      return new Either<>(left, null);
    }

    /**
     * Constructs a new right instance using the given type.
     *
     * @param right The right value to store.
     * @param <L> The type of left.
     * @param <R> The type of right.
     * @return Returns a new instance of Either.
     */
    public static <L, R> Either<L, R> right(R right) {
      return new Either<>(null, right);
    }

    /**
     * Apply a mapper function to either left or right.
     *
     * @param mapperLeft Mapping function for the left value.
     * @param mapperRight Mapping function for the right value.
     * @param <T> The return type of the left mapper.
     * @param <U> The return type of the right mapper.
     * @return Returns a new Either instance with the mapped value.
     */
    public <T, U> Either<T, U> map(Function<L, T> mapperLeft, Function<R, U> mapperRight) {
      return this.reduce(
          l -> Either.left(mapperLeft.apply(l)), r -> Either.right(mapperRight.apply(r)));
    }

    /**
     * Reduce left or right value to the same type.
     *
     * @param reducerLeft Reducer function for the left value.
     * @param reducerRight Reducer function for the right value.
     * @param <T> Type of the returned value.
     * @return Returns the reduced value.
     */
    public <T> T reduce(Function<L, T> reducerLeft, Function<R, T> reducerRight) {
      if (isLeft()) {
        return reducerLeft.apply(this.getLeft());
      } else {
        return reducerRight.apply(this.getRight());
      }
    }

    /**
     * @return True, if held type is of type left. False, otherwise.
     */
    public boolean isLeft() {
      return left != null;
    }

    /**
     * @return True, if held type is of type right. False, otherwise.
     */
    public boolean isRight() {
      return !this.isLeft();
    }

    /**
     * Return left value. Throws RuntimeException, if either is of type right.
     *
     * @return Returns the left type.
     */
    public L getLeft() {
      if (isLeft()) {
        return this.left;
      }
      throw new RuntimeException("Element is right");
    }

    /**
     * Return right value. Throws RuntimeException, if either is of type left.
     *
     * @return Returns the right type.
     */
    public R getRight() {
      if (isRight()) {
        return this.right;
      }
      throw new RuntimeException("Element is left");
    }

    @Override
    public String toString() {
      return reduce(l -> "Left: " + l, r -> "Right: " + r);
    }
  }

  /**
   * A class to cache expensive computations. The value is lazily evaluated once accessed.
   *
   * @param <T> Type of the value to compute.
   */
  public static class Cache<T> {

    private final Supplier<T> supplier;
    private T cachedValue = null;

    public Cache(Supplier<T> supplier) {
      this.supplier = supplier;
    }

    /**
     * Retrieve the cached value. Value is recalculated if not available yet.
     * @return Cached or calculated value.
     */
    public T get() {
      if (cachedValue == null) {
        this.cachedValue = supplier.get();
      }
      return cachedValue;
    }

    /**
     * Invalidate the cache to force a recomputation on the next access.
     */
    public void invalidate() {
      this.cachedValue = null;
    }
  }
}


