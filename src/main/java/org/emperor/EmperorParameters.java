package org.emperor;

public class EmperorParameters {

    public static final int SIMULATION_DEPTH = 5;
    public static final int SIMULATION_PACE_IN_MS = 5;
    public static final double EXPLOITATION_CONST = 1.5;
    public static final int DECISION_PACE_IN_MS = 1000;

  // thompson sampling
  public static final double THOMPSON_EXP_FACTOR = 0.99;

  // MCTS parameters
  public static final long MCTS_MAX_ADVANCE_STEPS = 100;
  public static final long MCTS_ADVANCE_PRECISION_MS = 1000;
  public static final long MCTS_TIME_LIMIT_MS = 250;
  public static final double MCTS_EXPLORATION_CONSTANT = Math.sqrt(2);

  // MapExplorerRollout parameters
  public final static long MAP_EXP_SIMULATION_PRECISION_MS = 1000;
  public final static long MAP_EXP_SIMULATION_DURATION_MS = 60_000;

  // SimulatedGame parameters

  public final static String SIM_PSEUDOGRAS_NAME = "Pseudogras";

  // computing parameters
  public static final int NUMBER_OF_THREADS = 1;
}
