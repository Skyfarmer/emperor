package org.emperor;

import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * The action database maintains a list of all busy units and cities.
 */
public class ActionDatabase {

  private static final Logger log = LogManager.getLogger(ActionDatabase.class);
  private final Set<UUID> busyUnits = new HashSet<>();
  private final Set<Position> producingCities = new HashSet<>();

  public ActionDatabase() {
  }

  private ActionDatabase(ActionDatabase db) {
    this.busyUnits.addAll(db.busyUnits);
    this.producingCities.addAll(db.producingCities);
  }

  /**
   * Create a deep copy of the database.
   *
   * @return A copy of the database.
   */
  public ActionDatabase copy() {
    return new ActionDatabase(this);
  }

  /**
   * Adds a unit, marking it as busy. If unit is already registered, it is not handled as an error.
   *
   * @param unit The UUID of the unit.
   */
  public void add(UUID unit) {
    if (!this.busyUnits.add(unit)) {
      log.warn("Unit {} already busy", unit);
    }
  }

  /**
   * Adds a city, marking it as busy. If city is already registered, it is not handled as an error.
   *
   * @param city The position of the city.
   */
  public void add(Position city) {
    if (!this.producingCities.add(city)) {
      log.warn("City {} already busy", city);
    }
  }

  /**
   * Removes a unit, marking it as available. If unit was not registered, it is not handled as an
   * error.
   *
   * @param unit The UUID of the unit.
   */
  public void remove(UUID unit) {
    if (!this.busyUnits.remove(unit)) {
      log.warn("Unit {} was not busy", unit);
    }
  }

  /**
   * Removes a city, marking it as available. If city was not registered, it is not handled as an
   * error.
   *
   * @param city The position of the city.
   */
  public void remove(Position city) {
    if (!this.producingCities.remove(city)) {
      log.warn("City {} was not busy", city);
    }
  }

  /**
   * Determine whether the given unit is available.
   *
   * @param uuid The UUID of the unit.
   * @return True if unit has not been registered. False, otherwise.
   */
  public boolean isAvailable(UUID uuid) {
    return !this.busyUnits.contains(uuid);
  }

  /**
   * Determine whether the given city is available.
   *
   * @param city The position of the city.
   * @return True if city has not been registered. False, otherwise.
   */
  public boolean isAvailable(Position city) {
    return !this.producingCities.contains(city);
  }
}
